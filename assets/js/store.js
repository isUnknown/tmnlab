const Store = {
  modals: {
    studyIntro: {
      isOpen: true,
      toggle: function() {
        this.isOpen = !this.isOpen
      }
    }
  },
  currentElement: null,
  setCurrentElement: function(element) {
    this.currentElement = element
  }
}

export default Store