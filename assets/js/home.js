import BlockCallout from "./components/blocks/block-callout.js"
import BlockGraph from "./components/blocks/block-graph.js"
import BlockHeading from "./components/blocks/block-heading.js"
import BlockQuote from "./components/blocks/block-quote.js"
import BlockSpec from "./components/blocks/block-spec.js"
import BlockTable from "./components/blocks/block-table.js"
import BlockText from "./components/blocks/block-text.js"
import BlockVs from "./components/blocks/block-vs.js"

const jsonUrl = `${window.location.pathname}home/data.json`

const headers = {
    'X-Requested-With': 'fetch',
}

fetch(jsonUrl, {
    method: 'GET',
    headers: headers
}).then(res => {
    if (res.ok) {
        return res.json()
    } else {
        throw new Error(res.status + ' — ' + res.statusText)
    }
}).then(fetchedData => {
    const blocks = fetchedData.blocks
    return blocks
}).then(blocks => {
    const app = new Vue({
        el: '#home',
        components: {
            'block-text': BlockText,
            'block-callout': BlockCallout,
            'block-quote': BlockQuote,
            'block-graph': BlockGraph,
            'block-table': BlockTable,
            'block-spec': BlockSpec,
            'block-vs': BlockVs,
            'block-heading': BlockHeading
        },
        data: {
            blocks: blocks
        }
    })
})
.catch(error => {
    console.log(error)
})