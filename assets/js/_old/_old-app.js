import Etabli from "./components/etabli.js";
import InteractiveScrollbar from "./components/interactive-scrollbar.js";
import EventBus from './eventBus.js'

const app = new Vue({
    el: '#app',
    components: {
        'etabli': Etabli,
        'interactive-scrollbar': InteractiveScrollbar
    },
    data: {
        tools: {
            isSelection: false,
        }
    },
    methods: {
        enableSelection: function() {
            this.tools.isSelection = !this.tools.isSelection
        },
        prepareDataElement: function(domElement) {
            const dataElement = {
                title: domElement.dataset.title,
                type: domElement.dataset.type,
                categories: []
            }

            domElement.querySelectorAll('.data-category').forEach(category => {
                dataElement.categories.push({
                    title: category.dataset.category,
                    value: parseInt(category.dataset.number)
                })
            })
            
            return dataElement
        },
        select: function(event) {
            if (!this.tools.isSelection) return
            
            const domElement = event.currentTarget
            domElement.classList.toggle('selected')

            EventBus.$emit('select', this.prepareDataElement(domElement))
        }
    },
    mounted: function() {
        EventBus.$on('enable-selection', () => {
            this.enableSelection()
        })
    }
})