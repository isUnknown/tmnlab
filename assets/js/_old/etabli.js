import EventBus from "../eventBus.js"
import ToolSelection from "../components/tool-selection.js"

const Etabli = {
    components: {
        'tool-selection': ToolSelection
    },
    data: function() {
        return {
            selection: []
        }
    },
    template: `
        <aside class="etabli" @mouseleave="shrink">
            <tool-selection>
            </tool-selection>
            <div v-for="item in selection" class="etabli__selected" @mouseover="expand">
                <span></span>
                <div class="item-title">{{ item.title }}</div>
            </div>
        </aside>
    `,
    methods: {
        isNewComponent: function(dataElement) {
            if (!this.selection.find(selectedElem => selectedElem.title === dataElement.title)) {
                return true
            } else {
                return false
            }
        },
        select: function(dataElement) {
            this.selection.push(dataElement)
        },
        unselect: function(dataElement) {
            this.selection = this.selection.filter(item => item.title !== dataElement.title)
        },
        expand: function() {
            if (this.selection.length > 0) {
                document.querySelector('.etabli').classList.add('etabli--expand')
            }
        },
        shrink: function() {
            if (this.selection.length > 0) {
                document.querySelector('.etabli').classList.remove('etabli--expand')
            }
        },
        processSelection: function() {
            EventBus.$on('select', (dataElement) => {
                if (this.isNewComponent(dataElement)) {
                    this.select(dataElement)
                } else {
                    this.unselect(dataElement)
                }
            })
        }
    },
    mounted: function() {
        this.processSelection()
    }
}

export default Etabli