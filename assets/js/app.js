import Study from "./components/study.js"
import PanelLeft from "./components/aside-left/panel-left.js"
import EventBus from './eventBus.js'
import PanelRight from "./components/aside-right/panel-right.js"
import WorkBench from "./components/work-bench/work-bench.js"
import MobileBanner from "./components/mobile-banner.js"
import studyIntro from "./components/study-intro.js"
import Tutorial from "./components/tutorial.js"

let jsonUrl = `${window.location.pathname}/data.json`

const headers = {
  'X-Requested-With': 'fetch',
}

fetch(jsonUrl, {
    method: 'GET',
    headers: headers
  }).then(res => {
    if (res.ok) {
      return res.json()
    } else {
      throw new Error(res.status + ' — ' + res.statusText)
    }
  }).then(fetchedData => {
    let Data = {
      'parts': fetchedData.parts,
      'flatIndex': fetchedData.flatIndex,
      'version': fetchedData.version
    }
    console.log('Study data: ', fetchedData)
    return Data
  }).then(Data => {
    const app = new Vue({
      el: '#app',
      components: {
        'study-intro': studyIntro,
        'tutorial': Tutorial,
        'study': Study,
        'panel-left': PanelLeft,
        'panel-right': PanelRight,
        'work-bench': WorkBench,
        'mobile-banner': MobileBanner
      },
      data: {
        parts: Data.parts,
        flatIndex: null,
        version: null,
        debug: false,
        isMobile: false,
        isPanelRightExpand: false,
        currentElement: null
      },
      methods: {
        expandPanelRight: function() {
          this.isPanelRightExpand = !this.isPanelRightExpand;
        },
        saveData: function () {
          console.log('App.saveData()')
        },
        versionControl: function () {
          const isStored = localStorage['version'] ? true : false
          let storedVersion = null
          let isDifferentVersion = null
          if (isStored) {
            storedVersion = localStorage['version']
            isDifferentVersion = (storedVersion !== undefined) && (storedVersion !== Data.version)
          }

          if (!isStored || isDifferentVersion) {
            if (this.debug) {
              console.log('(Re)init application')
            }
            this.flatIndex = Data.flatIndex
            this.version = Data.version
            localStorage.setItem('flatIndex', JSON.stringify(Data.flatIndex))
            localStorage.setItem('version', Data.version)
          } else {
            if (this.debug) {
              console.log('Load application from local storage.')
            }
            const storedApp = {
              flatIndex: JSON.parse(localStorage['flatIndex']),
              version: localStorage['version']
            }

            this.flatIndex = storedApp.flatIndex
            this.version = storedApp.version
          }
        },
        setIsMobile: function () {
          this.isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
        },
        closeMobileBanner: function () {
          this.isMobile = false
        }
      },
      created: function () {
        this.versionControl()
      },
      mounted: function () {
        this.setIsMobile()

        // Browser detection
        var browser = 'unknown';
        if ((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0) {
          browser = 'opera';
        } else if (navigator.userAgent.indexOf("Firefox") != -1) {
          browser = 'firefox';
        } else if (/constructor/i.test(window.HTMLElement) || (function (p) {
            return p.toString() === "[object SafariRemoteNotification]";
          })(!window['safari'] || (typeof safari !== 'undefined' && window['safari'].pushNotification))) {
          browser = 'safari';
        } else if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
          browser = 'ie';
        } else if (navigator.userAgent.indexOf("Edg") != -1) {
          browser = 'edge';
        } else if (!!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime)) {
          browser = 'chrome';
        }
        document.documentElement.dataset.browser = browser;
      }
    })
  })
  .catch(error => {
    console.log(error)
  })