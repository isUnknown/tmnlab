const iconMixin = {
    methods: {
        setIcon: function(type) {
            if (type === 'spec' || type === 'vs') {
                return 'callout'
            } else if (type === 'annex') {
                return 'part'
            } else {
                return type
            }
        }
    }
}

export default iconMixin