import BlockText from "./blocks/block-text.js"
import BlockCallout from "./blocks/block-callout.js"
import BlockQuote from "./blocks/block-quote.js"
import BlockGraph from "./blocks/block-graph.js"
import blockMixin from "./blocks/block-mixin.js"
import BlockTable from "./blocks/block-table.js"
import BlockSpec from "./blocks/block-spec.js"
import BlockVs from "./blocks/block-vs.js"
import BlockHeading from "./blocks/block-heading.js"
import BlockImage from "./blocks/block-image.js"
import Store from "../store.js"

const studyIntro = {
  components: {
    'block-text': BlockText,
    'block-callout': BlockCallout,
    'block-quote': BlockQuote,
    'block-graph': BlockGraph,
    'block-table': BlockTable,
    'block-spec': BlockSpec,
    'block-vs': BlockVs,
    'block-heading': BlockHeading,
    'block-image': BlockImage
  },
  data: function() {
    return {
      store: Store
    }
  },
  template: `
    <div v-if="store.modals.studyIntro.isOpen" class="container | print:hidden" id="study-intro">
        <div class="study home | mx-auto">
            <div class="part">
                <div class="block--title">
                    <h1>État des lieux du numérique 2021</h1>
                </div>
                <button class="btn btn--fill btn--study" @click="close">
                    Consulter l’étude <span>→</span>
                </button>
                <slot></slot>
            </div>
        </div>
    </div>
    `,
  methods: {
    close: function() {
      this.store.modals.studyIntro.toggle()
      document.querySelector('#btn-tutorial').classList.remove('opacity-0')
    },
    enableIntroButton: function() {
      document.querySelector('.intro__blocks').classList.remove('opacity-0')
      document.querySelector('#btn-intro').addEventListener('click', () => {
        this.store.modals.studyIntro.isOpen = !this.store.modals.studyIntro.isOpen;
        setTimeout(function() {
          document.querySelector('.intro__blocks').classList.remove('opacity-0')
        }, 100);
      });
    }
  },
  mounted: function() {
    document.querySelector('.intro__blocks').classList.remove('opacity-0')
    this.enableIntroButton();
  }
}

export default studyIntro