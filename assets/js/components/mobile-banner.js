const MobileBanner = {
    template: `
        <div class="mobile-banner">
          <p>Consultez la version ordinateur pour profiter de toutes les fonctionnalités.</p>
          <button 
            @click="close()"
            type="button"
            title="Fermer"
            class="btn btn--square btn--transparent"
          >
            <svg class="icon"><use xlink:href="#icon-close"></use></svg>
          </button>
        </div>
    `,
    methods: {
      close: function() {
        this.$emit('close-mobile-banner')
      }
    }
}

export default MobileBanner