import Subpart from "./subpart.js"
import EventBus from "../eventBus.js"
import blockMixin from "./blocks/block-mixin.js"
import BlockText from "./blocks/block-text.js"
import BlockCallout from "./blocks/block-callout.js"
import BlockQuote from "./blocks/block-quote.js"
import BlockGraph from "./blocks/block-graph.js"
import BlockTable from "./blocks/block-table.js"
import BlockSpec from "./blocks/block-spec.js"
import BlockVs from "./blocks/block-vs.js"
import BlockHeading from "./blocks/block-heading.js"
import BlockImage from "./blocks/block-image.js"

const Part = {
    mixins: [blockMixin],
    props: {
        part: Object
    },
    components: {
        'subpart': Subpart,
        'block-text': BlockText,
        'block-callout': BlockCallout,
        'block-quote': BlockQuote,
        'block-graph': BlockGraph,
        'block-table': BlockTable,
        'block-spec': BlockSpec,
        'block-vs': BlockVs,
        'block-heading': BlockHeading,
        'block-image': BlockImage
    },
    computed: {
        isAnnex: function() {
            return this.part.type === 'annex'
        }
    },
    template: `
        <div class="part" :class="{ 'part--annex': isAnnex }">
            <div 
              class="block block--part" :class="isAnnex ? 'block--annex': false" 
              :data-file-name="part.fileName"
              @mouseover="hover = true" 
              @mouseleave="hover = false" 
            >
                <h2 
                  :id="part.index.absolute" 
                  :data-id="part.index.absolute" 
                  class="observable" 
                  :data-part="setDataset()" 
                  :data-symbol="isAnnex ? '◇': false"
                >{{ part.title }}</h2>
                <actions :item="part" v-if="hover"></actions>
            </div>
            <component 
                v-for="block in part.blocks" 
                :is="'block-' + block.type" 
                :block="block" 
                :key="block.title + block.index.absolute"
                :parent-type="part.type"
                grand-parent-type="study"
            ></component>
            
            <subpart
                v-for="subpart in part.subparts"
                :key="subpart.title + subpart.index.absolute"
                :subpart="subpart"
                :parent-type="part.type"
                :part-index="part.index.inParent"
            >
            </subpart>
        </div>
    `,
    methods: {
        setDataset: function() {
          return this.part.index.roman
        }
    }
}

export default Part