import EventBus from '../../../eventBus.js'

const ToC = {
    props: {
        parts: Array
    },
    data: function() {
        return {
            bookmarks: []
        }
    },
    template: `
        <div class="toc | py-2">
            <h2 class="panel__item font-bold">Plan</h2>
            <div class="px-4">
                <ol>
                    <li v-for="part in parts" :key="part.title" class="summary__part">
                        <a :href="'#' + part.index.absolute" :class="{current: part.index.isCurrent}">{{ part.title }}</a>
                        <ol>
                            <li v-for="subpart in part.subparts"
                            :key="subpart.title"
                            @click="toggleMark(subpart.title, $event)">
                                <a :href="'#' + subpart.index.absolute" :class="{current: subpart.index.isCurrent}">{{ subpart.title }}</a>
                                
                            </li>
                        </ol>
                    </li>
                </ol>
            </div>
        </div>
    `,
    methods: {
        toggleMark: function(title, event) {
            const target = event.currentTarget
            if (target.classList.contains('bookmark')) {
                this.unBookmark(target, title)
            } else {
                this.Bookmark(target, title)
            }
        },
        bookmark: function(target, title) {
            target.classList.add('bookmark')
            this.bookmarks.push(title)
        },
        unBookmark: function(target, title) {
            target.classList.remove('bookmark')
            this.bookmarks = this.bookmarks.filter(bookmark => bookmark !== title)
        },
        isBookmarked: function(title) {
            return this.bookmarks.find(bookmark => bookmark === title) !== undefined
        },
        // MAJOR
        toggleInCollection(item) {
            // Send to collection
        },
        // MINOR
        comment(item) {
            // Scrolls to the item in the study component
            // Open the comment box
        },
        markAsCurrent(id) {
            this.unmarkAllAsCurrent()
            const target = this.selectItem(id)
            target.index.isCurrent = true
            
            if(target.ressources) {
                EventBus.$emit('change-ressources', target.ressources)
            } else {
                EventBus.$emit('change-ressources', [])
            }
        },
        unmarkAllAsCurrent() {
            this.parts.forEach(part => {
                part.index.isCurrent = false
                part.subparts.forEach(subpart => {
                    subpart.index.isCurrent = false
                    subpart.blocks.forEach(block => {
                        block.index.isCurrent = false
                    })
                })
            })
        },
        scrollTo(id) {
            // Scrollto Id
        },
        selectItem: function(id) {
            id = parseInt(id)
            
            let target = this.parts.find(part => part.index.absolute === id)
            
            if (target === undefined) {
                for (let i = 0; target === undefined && (i < this.parts.length) ; i++) {
                    target = this.parts[i].subparts.find(subpart => subpart.index.absolute === id)
                }
            }
            
            if (target === undefined) {
                this.parts.forEach(part => {
                    for (let i = 0; target === undefined && (i < part.subparts.length); i++) {
                        target = part.subparts[i].blocks.find(block => block.index.absolute === id)
                    }
                })
            }
            if (this.$root.debug) {
                console.log('toc.selectItem().target :', target)
            }
            return target
        }
    },
    mounted: function() {
        EventBus.$on('change-current-index', id => {
            this.markAsCurrent(id)
        })
    }
}

export default ToC