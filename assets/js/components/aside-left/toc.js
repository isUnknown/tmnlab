import EventBus from '../../eventBus.js'

const ToC = {
    props: {
        activeTab: String,
        flatIndex: Array
    },
    computed: {
        bookmarksIsEmpty: function() {
            return !this.flatIndex.some(item => item.isBookmarked)
        },
        currentIndex: function() {
          const currentPart = this.flatIndex.find(part => part.index.isCurrent)
          return currentPart.index.absolute
        }
    },
    template: `
        <div id="toc-panel" role="tabpanel" aria-labelledby="plan-tab" class="toc | py-2">
            <ol class="panel__list">
                <li v-for="item in flatIndex" :key="item.slug" v-if="activeTab === 'toc'"
                    :data-symbol="isAnnex(item) ? '◇': false" 
                    :data-part="isAnnex(item) ? false: getDataPart(item)" 
                    :data-subpart="setSubpart(item)" 
                    :data-subsubpart="item.index.heading ? indexToLetter(item.index.heading) : false" 
                    :class="{ bookmarked: item.isBookmarked, active: item.index.isCurrent }"
                >
                    <a :href="'#' + item.index.absolute">{{ item.title }}</a>
                    <button class="bookmark" :title="item.isBookmarked ? 'Retirer des signets' : 'Ajouter aux signets'" @click="toggleBookmark(item)">
                    <svg class="icon"><use :xlink:href="item.isBookmarked ? '#icon-bookmark-fill': '#icon-bookmark'" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg>
                    </button>
                </li>

                <li v-for="item in flatIndex" :key="item.slug" v-if="activeTab === 'bookmarks' && item.isBookmarked"
                    :data-symbol="isAnnex(item) ? '◇': false" 
                    :data-part="isAnnex(item) ? false: getDataPart(item)" 
                    :data-subpart="setSubpart(item)"
                    :data-subsubpart="item.index.heading ? indexToLetter(item.index.heading) : false"
                    :class="{ bookmarked: item.isBookmarked, 'active': item.index.isCurrent }"
                >
                    <a :href="'#' + item.index.absolute">{{ item.title }}</a>
                    <button class="bookmark" :title="item.isBookmarked ? 'Retirer des signets' : 'Ajouter aux signets'" @click="toggleBookmark(item)">
                    <svg class="icon"><use :xlink:href="item.isBookmarked ? '#icon-bookmark-fill': '#icon-bookmark'" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg>
                    </button>
                </li>
                <li v-if="bookmarksIsEmpty && activeTab === 'bookmarks'" class="empty"><p class="opacity-50">Aucun signet pour le moment.</p></li>
            </ol>
        </div>
    `,
    methods: {
        setSubpart: function(item) {
            if (this.isSubpart(item)) {
                return item.index.inParent
            } else if (item.index.subpart) {
                return item.index.subpart
            } else {
                return false
            }
        },
        getDataPart: function(item) {
            let dataPart
            if (this.isAnnex(item)) {
                return false
            } else if (item.type === 'part') {
                return dataPart = item.index.roman
            } else {
                return dataPart = item.index.romanPartParent
            }
        },
        isAnnex: function(item) {
            return item.type === 'annex' || item.index.isAnnex
        },
        isSubpart: function(item) {
            if (this.isAnnex(item)) {
                return false
            } else {
                return item.type === 'subpart'
            }
        },
        indexToLetter: function(index) {
            switch (index) {
                case 1:
                    return 'a'
                    break;
                case 2:
                    return 'b'
                    break;
                case 3:
                    return 'c'
                    break;
                case 4:
                    return 'd'
                    break;
                case 5:
                    return 'e'
                    break;
                case 6:
                    return 'f'
                    break;
                case 7:
                    return 'g'
                    break;
                case 8:
                    return 'h'
                    break;
                case 9:
                    return 'i'
                    break;
                case 10:
                    return 'j'
                    break;
                case 11:
                    return 'k'
                    break;
                case 12:
                    return 'l'
                    break;
                case 13:
                    return 'm'
                    break;
            }
        },
        toggleBookmark: function(item) {
            item.isBookmarked = !item.isBookmarked
        },
        isBookmarked: function(title) {
            return this.bookmarks.find(bookmark => bookmark === title) !== undefined
        },
        markAsCurrent(id) {
            this.unmarkAllAsCurrent()
            const target = this.selectItem(id)
            target.index.isCurrent = true

            this.$root.currentElement = target

            if (target.type === 'heading') {
              const parentSubpart = this.flatIndex.find(element => (element.type === 'subpart') && (element.index.romanPartParent === target.index.romanPartParent) && (element.index.inParent === target.index.subpart))
              EventBus.$emit('change-ressources', parentSubpart.ressources)
            } else if(target.ressources) {
                EventBus.$emit('change-ressources', target.ressources)
            } else {
              EventBus.$emit('change-ressources', [])
            }
        },
        unmarkAllAsCurrent() {
            this.flatIndex.forEach(item => {
                item.index.isCurrent = false
            })
        },
        selectItem: function(id) {
            id = parseInt(id)
            
            let target = this.flatIndex.find(item => item.index.absolute === id)
            if (this.$root.debug) {
                console.log('toc.selectItem().target :', target)
            }
            return target
        }
    },
    mounted: function() {
        EventBus.$on('change-current-index', id => {
            this.markAsCurrent(id)
        })
    },
    updated: function() {
        const jsonFlatIndex = JSON.stringify(this.flatIndex)
        localStorage.setItem('flatIndex', jsonFlatIndex)
    }
}

export default ToC