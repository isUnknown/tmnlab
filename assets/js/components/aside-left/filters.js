import EventBus from "../../eventBus.js"

const Filters = {
    props: {
        filters: Array
    },
    template: `
    <div class="aside-left__filters" style="display: none;">
        <div class="filter" v-for="filter in filters">
            <h1 class="filter__name">{{ filter.name }}</h1>
            <ul class="filter__options">
                <li v-for="option in filter.options" @click="toggleFilter(filter.name, option)">
                    <h2>{{ option }}</h2>
                </li>
            </ul>
        </div>
    </div>
    `,
    methods: {
        toggleFilter: function(category, option) {
            const item = {
                category: category, 
                option: option
            }
            EventBus.$emit('toggleFilter', item)
        }
    }
}

export default Filters