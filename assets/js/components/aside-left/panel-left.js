import Filters from "./filters.js"
import ToC from "./toc.js"

const PanelLeft = {
    props: {
        parts: Array,
        flatIndex: Array
    },
    components: {
        'toc': ToC,
    },
    data: function() {
        return {
            'activeTab': 'toc'
        }
    },
    computed: {
        bookmarkIcon: function() {
            const icon = this.activeTab === 'bookmarks' ? '#icon-bookmark-fill' : '#icon-bookmark'
            return icon
        }
    },
    template: `
        <div class="panel panel--left | border-r print:hidden">
            <header 
                :data-selected-tab="activeTab + '-tab'"
                role="tablist" 
                aria-orientation="horizontal" 
                class="border-b"
            >
                <button role="tab" :aria-selected="activeTab === 'bookmarks' ? true: false" id="bookmarks-tab" aria-controls="toc-panel" title="Signets" @click="switchTab('bookmarks')">
                    <svg class="icon"><use :xlink:href="bookmarkIcon" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg>
                </button>
                <button class="border-l" role="tab" :aria-selected="activeTab === 'toc' ? 'true': false" id="toc-tab" aria-controls="toc-panel" @click="switchTab('toc')">Table des matières</button>
            </header>
            <toc
                :parts="parts"
                :flat-index="flatIndex"
                :active-tab="activeTab"
            >
            </toc>
        </div>
    `,
    methods: {
        switchTab: function(target) {
            this.activeTab = target
        }
    }
}

export default PanelLeft