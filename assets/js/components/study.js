import EventBus from "../eventBus.js"
import Note from "./note.js"
import Part from "./part.js"

const Study = {
    props: {
        parts: Array
    },
    components: {
        'part': Part,
        'note': Note
    },
    template: `
        <article class="container | print:hidden">
            <div class="study | mx-auto">
                <div class="block--title">
                    <h1>État des lieux du numérique 2021</h1>
                </div>
                <part v-for="part in parts" 
                    :key="part.title"
                    :part="part">
                </part>
                <note>
                </note>
            </div>
        </article>
    `,
    methods: {
        observe: function() {
            const minVisible = 0.02
            let observer = new IntersectionObserver(observables => {
                observables.forEach(observable => {
                    if (observable.intersectionRatio > minVisible) {
                        if (this.$root.debug) {
                            console.log(this.$options._componentTag + ' observe: ', observable)
                        }
                        this.sendCurrentIndex(observable.target.dataset.id)
                    }
                })
            }, {
                // threshold: minVisible,
                rootMargin: "-10% 0px -80% 0px"
            })
    
            let targets = document.querySelectorAll('.observable')
            targets.forEach(target => {
                observer.observe(target)
            })
        },
        sendCurrentIndex: function(id) {
            EventBus.$emit('change-current-index', id)
        }
    },
    created: function() {
        EventBus.$on('toggleFilter', item => {
            this.toggleFilter(item)
        })
    },
    mounted: function() {
        this.observe()
    }
}

export default Study