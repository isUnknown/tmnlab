import EventBus from "../eventBus.js"
import BlockText from "./blocks/block-text.js"
import BlockCallout from "./blocks/block-callout.js"
import BlockQuote from "./blocks/block-quote.js"
import BlockGraph from "./blocks/block-graph.js"
import blockMixin from "./blocks/block-mixin.js"
import BlockTable from "./blocks/block-table.js"
import BlockSpec from "./blocks/block-spec.js"
import BlockVs from "./blocks/block-vs.js"
import BlockHeading from "./blocks/block-heading.js"
import BlockImage from "./blocks/block-image.js"

const Subpart = {
    mixins: [blockMixin],
    props: {
        subpart: Object,
        parentType: String,
        partIndex: Number
    },
    components: {
        'block-text': BlockText,
        'block-callout': BlockCallout,
        'block-quote': BlockQuote,
        'block-graph': BlockGraph,
        'block-table': BlockTable,
        'block-spec': BlockSpec,
        'block-vs': BlockVs,
        'block-heading': BlockHeading,
        'block-image': BlockImage
    },
    template: `
        <div class="subpart">
            <div class="block block--subpart" @mouseover="hover = true" @mouseleave="hover = false" :data-file-name="subpart.fileName">
                <h3 
                    class="observable"
                    :id="subpart.index.absolute" 
                    :data-id="subpart.index.absolute" 
                    :data-index="subpart.index.inParent" 
                    :data-part="subpart.index.romanParent" 
                    :data-subpart="subpart.index.inParent"
                >
                    {{ subpart.title }}
                </h3>
                <actions :item="subpart" v-if="hover"></actions>
            </div>
            
            <component v-for="block in subpart.blocks"
                :is="'block-' + block.type" 
                :block="block" 
                :key="block.title + block.index.absolute" 
                :parent-type="subpart.type"
                :grand-parent-type="parentType"
                :subpart-index="subpart.index.inParent"
                :part-index="partIndex"
            ></component>
        </div>
    `
}

export default Subpart