import EventBus from "../eventBus.js"
import Store from "../store.js"

const Tutorial = {
  data: function() {
    return {
      store: Store,
      isOpen: false,
      steps: [
        {
          title: 'Table des matières',
          description: 'La table des matières permet de naviguer au sein des différentes parties de l’étude. Cliquez sur un titre pour y accéder.',
          image: 'assets/images/tutorial/1-toc.png',
          index: 1,
          arrow: {
            orientation: 'left',
            position: 'top'
          },
          position: {
            top: '74px',
            left: '280px',
            bottom: 'unset',
            right: 'unset'
          }
        },
        {
          title: 'Ajouter aux signets',
          description: 'Pour ajouter une partie aux signets, cliquez sur l’icône qui apparait au survol. Cliquez de nouveau pour la supprimer des signets.',
          image: 'assets/images/tutorial/2-ajouter-aux-signets.png',
          index: 2,
          arrow: {
            orientation: 'top',
            position: 'left'
          },
          position: {
            top: '162px',
            left: '1px',
            bottom: 'unset',
            right: 'unset'
          },
          focus: '.toc li:first-child .bookmark'
        },
        {
          title: 'Signets',
          description: 'Cliquez sur ce bouton pour voir tous vos signets.',
          image: 'assets/images/tutorial/3-signets.png',
          index: 3,
          arrow: {
            orientation: 'left',
            position: 'top'
          },
          position: {
            top: '74px',
            left: '41px',
            bottom: 'unset',
            right: 'unset'
          },
          focus: '#bookmarks-tab'
        },
        {
          title: 'Ajouter à ma collecte',
          description: 'Au survol de n\'importe quel élément de l\étude, cliquez sur cette icône pour l’ajouter à votre collecte.',
          image: 'assets/images/tutorial/4-ajouter-a-ma-collecte.png',
          index: 4,
          arrow: {
            orientation: 'top',
            position: 'right'
          },
          position: {
            top: '200px',
            left: 'unset',
            bottom: 'unset',
            right: 'calc(-346px + 50vw)'
          },
          scroll: '.block--question',
          focus: '.block--actions'
        },
        {
          title: 'Ma collecte',
          description: 'Votre collecte se trouve ici. Les données sont stockées localement sur votre ordinateur, vous retrouverez votre collecte même après avoir fermé la page.',
          image: 'assets/images/tutorial/5-ma-collecte.png',
          index: 5,
          arrow: {
            orientation: 'right',
            position: 'top'
          },
          position: {
            top: '74px',
            left: 'unset',
            bottom: 'unset',
            right: '280px'
          }
        },
        {
          title: '(Re)travailler ma collecte',
          description: '(Re)travaillez votre collecte pour en réordonner les éléments, l\'exporter entièrement en PDF ou exporter séparément les graphiques.',
          image: 'assets/images/tutorial/6-retravailler-ma-collecte.png',
          index: 6,
          arrow: {
            orientation: 'right',
            position: 'top'
          },
          position: {
            top: '143px',
            left: 'unset',
            bottom: 'unset',
            right: '280px'
          },
          focus: '.collection button'
        },
        {
          title: 'Ressources',
          description: 'Retrouvez ici les ressources associées à la sous-partie que vous lisez. Ouvrez le panneau pour voir toutes les ressources de l\'étude et les exporter.',
          index: 7,
          arrow: {
            orientation: 'right',
            position: 'bottom'
          },
          position: {
            top: 'unset',
            left: 'unset',
            bottom: '1px',
            right: '280px'
          }
        }
      ],
      currentIndex: 1
    }
  },
  watch: {
    isOpen: function(val) {
      if (val === true) {
        setTimeout(() => {
          this.focusPrev()
        }, 100)
      }
    },
    currentStep: function() {
      if (this.currentStep.scroll) {
        document.querySelector(this.currentStep.scroll).scrollIntoView();
      }
      if (this.currentStep.focus) {
        if (this.currentStep.focus === '.block--actions') {
          EventBus.$emit("enableFocus");
        }
        else {
          EventBus.$emit("disableFocus");
        }
        document.querySelector(this.currentStep.focus).focus()
      }
    }
  },
  computed: {
    isFirstStep: function() {
      const isFirstStep = this.currentIndex === 1
      return isFirstStep
    },
    isLastStep: function() {
      const isLastStep = this.currentIndex === this.steps.length
      return isLastStep
    },
    currentStep: function() {
      const currentStep = this.steps.find(step => step.index === this.currentIndex)
      return currentStep
    }
  },
  template: `
    <dialog 
      id="tutorial" 
      role="dialog" 
      aria-labelledby="tutorial-label" 
      aria-describedby="tutorial-description" 
      aria-modal="true" open="open" 
      class="dialog" 
      :style="'--top:' + currentStep.position.top + '; --left:' + currentStep.position.left + '; --bottom: ' + currentStep.position.bottom + '; --right: ' + currentStep.position.right"
      :data-image="currentStep.image ? 'true' : 'false'"
      v-if="isOpen && !store.modals.studyIntro.isOpen"
    >
      <div :class="'arrow arrow--' + currentStep.arrow.orientation + '-' + currentStep.arrow.position"></div>
      <header class="dialog__header | border-b print:hidden">
        <h2 id="tutorial-label">{{ currentStep.title }}</h2>
        <button 
          @click="close"
          type="button" 
          id="btn-tutorial-close" 
          value="close" 
          title="Fermer" 
          class="btn btn--square btn--transparent"
        >
          <svg class="icon"><use xlink:href="#icon-close" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg> <span class="sr-only">Fermer</span>
        </button>
      </header>
      <div class="dialog__body">
        <img v-if="currentStep.image" :src="currentStep.image" alt="" width="300" height="150" class="bg-gray-50 border-b">
        <p id="tutorial-description" class="panel__item">{{ currentStep.description }}</p>
      </div>
      <footer class="dialog__footer">
        <p class="counter">{{ currentIndex }} sur {{ steps.length }}</p>
        <div>
          <button 
            @click="prev"
            class="btn btn--outline"
          >
            {{ isFirstStep ? 'Passer': 'Précédent' }}
          </button>
          <button 
            @click="next"
            class="btn btn--fill"
          >
            {{ isLastStep ? 'fermer': 'Suivant' }}
          </button>
        </div>
      </footer>
    </dialog>
  `,
  methods: {
    next: function() {
      if (this.isLastStep) {
        this.close()
      } else {
        this.currentIndex++
      }
    },
    prev: function() {
      if (this.isFirstStep) {
        this.close()
      } else {
        this.currentIndex--
      }
    },
    close: function() {
      this.currentIndex = 1
      this.isOpen = false
      document.querySelector('#btn-tutorial').focus()
      window.localStorage.setItem('tutorial', 'seen')
    },
    enableHelpButton: function() {
      document.querySelector('#btn-tutorial').addEventListener('click', () => {
        this.isOpen = !this.isOpen
      })
    },
    enableEscapeKey: function() {
      document.addEventListener('keyup', e => {
        if (e.key === 'Escape') {
          if (this.isOpen === true) {
            this.isOpen = false
          }
        }
      })
    },
    enableLeftKey: function() {
      document.addEventListener('keyup', e => {
        if (e.key === 'ArrowLeft') {
          if (this.isOpen === true && !this.isFirstStep) {
            this.prev()
          }
        }
      })
    },
    enableRightKey: function() {
      document.addEventListener('keyup', e => {
        if (e.key === 'ArrowRight') {
          if (this.isOpen === true && !this.isLastStep) {
            this.next()
          }
        }
      })
    },
    focusPrev: function() {
      document.querySelector('#tutorial .dialog__footer button').focus()
    }
  },
  beforeMount: function() {
    if (window.localStorage.getItem('tutorial') !== 'seen') {
      this.isOpen = true
    }
  },
  mounted: function() {
    this.enableHelpButton()
    this.enableEscapeKey()
    this.enableRightKey()
    this.enableLeftKey()
  }
}

export default Tutorial