import blockMixin from "./block-mixin.js"

const BlockHeading = {
    mixins: [blockMixin],
    props: {
        block: Object,
        grandParentType: String,
        parentType: String,
        subpartIndex: Number,
        partIndex: Number
    },
    computed: {
        isObservable: function() {
            return this.parentType === 'subpart'
        }
    },
    template: `
        <h4 v-if="block" v-html="block.text" 
            :data-subsubpart="block.calloutBlock ? false : indexToLetter(block.index.heading)" 
            :data-subpart="block.calloutBlock ? false : subpartIndex" 
            :data-part="block.calloutBlock ? false : partIndex" 
            :id="block.calloutBlock ? false : block.index.absolute" 
            :data-id="block.calloutBlock ? false : block.index.absolute"
            :class="{ observable: isObservable}"
            ref="block"
        ></h4>
    `,
    methods: {
        indexToLetter: function(index) {
            switch (index) {
                case 1:
                    return 'a'
                    break;
                case 2:
                    return 'b'
                    break;
                case 3:
                    return 'c'
                    break;
                case 4:
                    return 'd'
                    break;
                case 5:
                    return 'e'
                    break;
                case 6:
                    return 'f'
                    break;
                case 7:
                    return 'g'
                    break;
                case 8:
                    return 'h'
                    break;
                case 9:
                    return 'i'
                    break;
                case 10:
                    return 'j'
                    break;
                case 11:
                    return 'k'
                    break;
                case 12:
                    return 'l'
                    break;
                case 13:
                    return 'm'
                    break;
            }
        }
    }
}

export default BlockHeading