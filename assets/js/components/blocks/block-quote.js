import EventBus from "../../eventBus.js"
import blockMixin from "./block-mixin.js"

const BlockQuote = {
    mixins: [blockMixin],
    props: {
        block: Object
    },
    template: `
        <figure :class="block.calloutBlock ? 'block--quote' : 'block block--quote'" @mouseover="hover = true" @mouseleave="hover = false" :data-file-name="block.fileName">
            <blockquote>
                <p>{{ block.html }}</p>
            </blockquote>
            <figcaption>{{ block.author }}</figcaption>
            <actions :item="block" v-if="hover && !isHome"></actions>
        </figure>
    `
}

export default BlockQuote