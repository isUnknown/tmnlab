const listMixin = {
    computed : {
        div: function() {
            let html = document.createElement('div')
            html.innerHTML = this.block.html
            return html
        },
        containsList: function() {
            const html = this.div            
            
            if (html.querySelectorAll('p').length <= 1) {
                return false
            } else {
                const pArray = Array.prototype.slice.call(html.querySelectorAll('p'))
                const startByHyphen = /^-/
                return pArray.some(p => startByHyphen.test(p.innerText))
            }
        },
        listedHTML: function() {
            if (this.containsList) {
                const source = this.div
                const paragraphNbr = this.div.querySelectorAll('p').length
                const startByHyphen = /^-/
                let listedNode = document.createElement('div')
                
                let isRunningList = false
                
                let ulNode = document.createElement('ul')
                
                source.querySelectorAll('p').forEach((p, key) => {
                    key = key + 1
                    const content = p.innerHTML
                    const isLi = startByHyphen.test(content)
                    if (isLi) {
                        if (isRunningList === false) {
                            isRunningList = true
                            ulNode = document.createElement('ul')
                        }
                        let liNode = document.createElement('li')
                        liNode.innerHTML = content.substring(2)
                        ulNode.appendChild(liNode)
                        if (key === paragraphNbr) {
                            listedNode.appendChild(ulNode)
                            isRunningList = false
                        }
                    } else {
                        if (isRunningList === true) {
                            isRunningList = false
                            listedNode.appendChild(ulNode)
                        }
                        listedNode.appendChild(p)
                    }
                })
                return listedNode.outerHTML
            } else {
                return false
            }
        }
    }
}

export default listMixin