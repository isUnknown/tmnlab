import EventBus from "../../eventBus.js"
import Actions from "./actions.js"

const blockMixin = {
    props: {
        isHome: Boolean
    },
    data: function() {
        return {
            hover: false
        }
    },
    components: {
        'actions': Actions
    },
    methods: {
        collect: function(item) {
            EventBus.$emit('collect', item)
        }
    }
}

export default blockMixin