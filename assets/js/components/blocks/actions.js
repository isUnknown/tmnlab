import EventBus from "../../eventBus.js"

const Actions = {
    props: {
        item: Object
    },
    template: `
    <div class="block--actions">
        <button class="btn btn--item btn--transparent" title="Ajouter à ma collecte" @click="collect(item)">
            <svg class="icon"><use xlink:href="#icon-collect" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg>
        </button>
    </div>
    `,
    methods: {
        collect: function(item) {
            EventBus.$emit('collect', item)
        },
        openNote: function() {
            EventBus.$emit('open-note', this.item.index.absolute)
        }
    }
}

export default Actions