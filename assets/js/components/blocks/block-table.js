import blockMixin from "./block-mixin.js"

const BlockTable = {
    mixins: [blockMixin],
    props: {
        block: Object
    },
    computed: {
        columnsNbr: function() {
            return this.block.lines[0].length - 1
        },
        head: function() {
            const head = this.block.lines[0].slice(1, this.block.lines[0].length)
            return head
        },
        body: function() {
            const body = this.block.lines.slice(1, this.block.lines.length)
            return body
        },
        isOverflow: function() {
            return this.columnsNbr > 5
        }
    },
    template: `
        <div class="question-container" :class="isOverflow ? 'question-container--overflow': false" :data-file-name="block.fileName">
            <div class="block block--question" @mouseover="hover = true" @mouseleave="hover = false">
                <h4 class="question">{{ block.title }}</h4>
                <actions :item="block"></actions>
            </div>
            <div class="block block--table" @mouseover="hover = true" @mouseleave="hover = false">
                <div class="table-container" :data-overflow="isOverflow ? true : false">
                    <table>
                        <thead>
                            <tr>
                                <th></th>
                                <th scope="col" v-for="(cell, cellIndex) in head" :key="cell + cellIndex">{{ cell }}</th>
                            </tr>                
                        </thead>
                        <tbody>
                            <tr v-for="(line, lineIndex) in body" :key="line[0] + '_' + lineIndex + '_' + block.index.absolute" scope="row">
                                <template v-for="(cell, cellIndex) in line">
                                    <th scope="row" v-if="cellIndex === 0">{{ cell }}</th>
                                    <td v-else-if="cell !== ''" :style="'--opacity:' + opacity(cell)">{{ cell }}%</td>
                                    <td v-else></td>
                                </template>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <actions v-if="hover && !isHome && isNotLargeTable()" :item="block"></actions>
            </div>
        </div>
    `,
    methods: {
        opacity: function(cell) {
            let opacity = cell/100
            return opacity
        },
        isNotLargeTable: function() {
          return !this.block.lines || this.block.lines.length < 6
        }
    }
}

export default BlockTable