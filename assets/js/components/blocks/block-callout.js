import EventBus from "../../eventBus.js"
import BlockHeading from "./block-heading.js"
import BlockImage from "./block-image.js"
import blockMixin from "./block-mixin.js"
import BlockQuote from "./block-quote.js"
import BlockText from "./block-text.js"
import listMixin from "./list-mixin.js"

const BlockCallout = {
    mixins: [blockMixin, listMixin],
    props: {
        block: Object
    },
    components: {
        'block-heading': BlockHeading,
        'block-text': BlockText,
        'block-quote': BlockQuote,
        'block-image': BlockImage
    },
    template: `
        <div 
            class="block block--callout"
            @mouseover="hover = true"
            @mouseleave="hover = false"
            :data-file-name="block.fileName"
        >
            <component v-for="calloutBlock in block.blocks" 
                :is="'block-' + calloutBlock.type" 
                :block="calloutBlock" 
                :key="calloutBlock.title"
                :parent-type="block.type"
            ></component>
            <actions :item="block" v-if="hover && !isHome"></actions>
        </div>
    `
}

export default BlockCallout