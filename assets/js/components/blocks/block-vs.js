import blockMixin from './block-mixin.js'
import listMixin from './list-mixin.js'

const BlockVs = {
    mixins: [blockMixin, listMixin],
    props: {
        block: Object
    },
    template: `
    <div class="block block--callout block--vs" @mouseover="hover = true" @mouseleave="hover = false" :data-file-name="block.fileName">
        <h4>VS 2016</h4>
        <div v-html="block.html"></div>
        <actions :item="block" v-if="hover && !isHome"></actions>
    </div>
    `
}

export default BlockVs