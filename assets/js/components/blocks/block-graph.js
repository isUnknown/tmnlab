import blockMixin from "./block-mixin.js"
import EventBus from "../../eventBus.js"

const BlockGraph = {
    mixins: [blockMixin],
    props: {
        block: Object
    },
    data: function() {
        return {
            isFocused: false
        }
    },
    computed: {
        columnsNbr: function() {
            return this.block.lines[0].length - 1
        },
        options: function() {
            if (this.block.variant === '100-simple') {
                let options = []
                this.block.lines.forEach(line => {
                    options.push(line[0])
                })
                return options
            } else {
                return this.block.lines[0].slice(1, this.block.lines[0].length)
            }
        },
        statements: function() {
            if (this.block.variant === '100-multiple') {
                return this.block.lines.slice(1, this.block.lines.length)
            } else {
                return this.block.lines
            }
            
        },
        is100Simple: function() {
            return this.block.variant === '100-simple'
        },
        is100Multiple: function() {
            return this.block.variant === '100-multiple'
        }
    },
    template: `
    <div class="question-container" :data-file-name="block.fileName">
        <div class="block block--question"  @mouseover="hover = true" @mouseleave="hover = false">
            <h4>{{ block.title }}</h4>
            <actions :item="block" v-if="(hover && !isHome) || isFocused"></actions>
        </div>
        <figure class="block block--graph" :class="'graph--' + block.variant"  @mouseover="hover = true" @mouseleave="hover = false">
            <div class="graph__legends" :style="setStyle()" v-if="block.variant !== 'multiple'">
                <p class="graph__legend" v-for="(option, index) in options" :key="option" :data-value="is100Simple ? statements[index][1]: false">
                    <span class="graph__unit" :class="'bg-' + color(index)"></span> {{ option }}
                </p>
            </div>
            <div class="graph graph--bars">
                <div v-if="is100Simple" class="graph__label-bar" :key="block.title + block.index.absolute"> 
                    
                    <div class="graph__bar" :aria-label="ariaLabel()"> 
                        <template v-for="(value, valueIndex) in block.lines">
                            <span v-for="index in parseInt(value[1])" class="graph__unit" :class="'bg-' + color(valueIndex)" :data-value="value"></span>
                        </template>
                    </div>
                </div>
                
                <div v-else v-for="(statement, statementIndex) in statements" class="graph__label-bar" :key="statement[0] + statementIndex"> 
                    <p class="graph__label">{{ statement[0] }}</p>
                    
                    <div class="graph__bar" :aria-label="ariaLabel(statement)"> 
                        <template v-for="(value, valueIndex) in setStatement(statement)">
                            <span v-for="index in setValue(value)" class="graph__unit" :class="'bg-' + color(valueIndex)" :data-value="value" :data-group="is100Multiple ? valueIndex + 1 : false"></span>
                        </template>
                    </div>
                </div>
            </div>
            <actions :item="block" v-if="hover && !isHome"></actions>
        </figure>
    </div>
    `,
    methods: {
        color: function(index) {
            let color
            if (this.is100Simple) {
                switch (index) {
                    case 0:
                        color = 'orange'
                        break;
                    case 1:
                        color = 'blue'
                        break;
                    case 2:
                        color = 'purple'
                        break;
                    case 3:
                        color = 'pink'
                        break;
                }
            } else {
                switch (index) {
                    case 0:
                        color = 'blue'
                        break;
                    case 1:
                        color = 'blue-light'
                        break;
                    case 2:
                        color = 'gray-300'
                        break;
                    case 3:
                        color = 'orange-light'
                        break;
                    case 4:
                        color = 'orange'
                        break;
                    case 5:
                        color = 'purple'
                        break;
                }
            }
            
            return color
        },
        ariaLabel: function(statement = null) {
            let ariaLabel = ''
            if (this.is100Simple) {
                this.block.lines.forEach((line, index) => {
                    const option = line[0].toLowerCase()
                    const value = line[1]
                    const isLastLine = index === this.block.lines.length - 1
                    
                    if (!isLastLine) {
                        ariaLabel += `${option} pour ${value}%, `
                    } else {
                        ariaLabel += `${option} pour ${value}%.`
                    }
                })
            } else {
                const values = this.is100Simple ? this.block.lines : statement.slice(1, statement.length)

                values.forEach((value, index) => {
                    if (Number.isInteger(value)) {
                        const option = this.options[index]
                        if (index !== values.length - 1) {
                            ariaLabel += `${value}% ${option.toLowerCase()}, `
                        } else {
                            ariaLabel += `${value}% ${option.toLowerCase()}.`
                        }
                    }
                })
            }

            return ariaLabel
        },
        setStatement: function(statement) {
            if (this.block.variant === '100-simple') {
                return statement
            } else {
                return statement.slice(1, statement.length)
            }
        },
        setValue: function(value) {
            let newValue = 0
            if (value === "") {
                return newValue
            } else {
                return parseInt(value)
            }
        },
        setStyle: function() {
            if (this.is100Simple) {
                return '--columns:1'
            } else {
                switch (this.columnsNbr) {
                    case 2:
                        return '--columns:2'
                        break;
                    
                    case 4:
                        return '--columns:4'
                        break;
                    
                    default:
                        return '--columns:3'
                        break;
                }
            }
        }
    },
    mounted: function() {
        EventBus.$on("enableFocus", function() {
            this.isFocused = true
        })
        EventBus.$on("disableFocus", function() {
            this.isFocused = false
        })
    }
}

export default BlockGraph