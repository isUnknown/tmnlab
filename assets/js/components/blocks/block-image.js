import blockMixin from "./block-mixin.js"

const BlockImage = {
    mixins: [blockMixin],
    props: {
        block: Object
    },
    template: `
        <figure>
            <img :src="block.src" :alt="block.alt">
        
            <figcaption 
                v-if="block.caption.length > 0" 
                v-html="block.caption"
            ></figcaption>
        </figure>
    `
}

export default BlockImage