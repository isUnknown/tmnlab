import EventBus from "../../eventBus.js"
import blockMixin from "./block-mixin.js"
import listMixin from "./list-mixin.js"

const BlockText = {
    mixins: [blockMixin, listMixin],
    props: {
        block: Object
    },
    template: `
        <div :class="block.calloutBlock ? 'block--text' : 'block block--text'" @mouseover="hover = true" @mouseleave="hover = false" :data-file-name="block.fileName">
            <div v-html="listedHTML ? listedHTML: block.html">
                
            </div>
            <actions :item="block" v-if="hover && !isHome"></actions>
        </div>
    `
}

export default BlockText