import blockMixin from './block-mixin.js'
import listMixin from './list-mixin.js'

const BlockSpec = {
    mixins: [blockMixin, listMixin],
    props: {
        block: Object
    },
    template: `
    <div class="block block--callout block--spec" @mouseover="hover = true" @mouseleave="hover = false" :data-file-name="block.fileName">
        <h4>Spécificités à noter</h4>
        <div v-html="block.html"></div>
        <actions :item="block" v-if="hover && !isHome"></actions>
    </div>
    `
}

export default BlockSpec