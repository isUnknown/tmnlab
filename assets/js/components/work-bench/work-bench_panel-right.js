import Palette from "./palette.js"
import PDFInfos from "./pdf-infos.js"


let print = () => {
  const doc = new jsPDF()
}
const PanelRight = {
    props: {
        scriptZipMultipleUrl: String,
        scriptTmpDeleteUrl: String,
        collection: Array
    },
    components: {
        'palette': Palette,
        'pdf-infos': PDFInfos
    },
    data: function() {
        return {
            exportOption: 'multiple',
            inProgress: false,
            isOpenPDFInfos: false
        }
    },
    computed: {
        isEmptyGraphs: function() {
            const typesHasGraphs = ['graph', 'table', 'part', 'subpart']
            return !this.collection.some(item => ((typesHasGraphs.includes(item.type)) && item.isVisible))
        }
    },
    template: `
    <div class="panel panel--right | border-l print:hidden">
        <pdf-infos v-if="isOpenPDFInfos" @close="closePDFInfos"></pdf-infos>
        
        <div>
            <!-- <palette></palette> -->
            <!-- <section class="py-2 border-b">
                <h3 class="panel__item | font-bold">Affichage</h3>
                <div class="panel__item panel__item--checkbox">
                    <input type="checkbox" id="comments" name="comments" checked>
                    <label for="comments">Annotations</label>
                </div>
                <div class="panel__item panel__item--label-btn">
                    <span>Taille des éléments</span>
                    <div class="flex items-center">
                        <button type="button" id="minus" class="btn btn--item" title="Diminuer">
                            <svg class="icon"><use xlink:href="#icon-minus" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg>
                        </button>
                        <button type="button" id="plus" class="btn btn--item" title="Augmenter">
                            <svg class="icon"><use xlink:href="#icon-plus" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg>
                        </button>
                    </div>
                </div>
            </section> -->
        </div>
        
        <section class="py-2 border-t">
            <h3 class="panel__item | font-bold">Export</h3>
            <div class="panel__item panel__item--radio">
                <input type="radio" id="png" name="export" value="multiple" v-model="exportOption">
                <label for="multiple">Exporter les graphs (.png)</label>
            </div>
            <div class="panel__item panel__item--radio">
                <input type="radio" id="pdf" name="export" value="simple" v-model="exportOption">
                <label for="simple">Export PDF (bêta)</label>
            </div>
            <div class="panel__item">
                <button 
                    id="btn-export"
                    :class="inProgress ? 'btn--loading': false"
                    class="btn btn--fill ld-ext-right"
                    @click="handleExport"
                    :disabled="this.inProgress || this.isEmptyGraphs"
                >
                    {{ isEmptyGraphs ? 'Aucun graph à exporter' : 'Exporter'}}
                    <div class="ld ld-spin ld-ring"></div>
                </button>
            </div>
        </section>
    </div>
    `,
    methods: {
        handleExport: function() {
            if (this.exportOption === 'simple') {
                this.isOpenPDFInfos = true
            } else {
                this.inProgress = true
                setTimeout(() => {
                    this.exportMultiple()
                }, 100);
            }
        },
        closePDFInfos: function() {
            this.isOpenPDFInfos = false
        },
        exportMultiple: function() {
            this.inProgress = true
            setTimeout(() => {
                const targets = document.querySelectorAll('.work .question-container')
                let files = []
                
                targets.forEach(item => {
                    html2canvas(item,{allowTaint : true})
                    .then(function(canvas) {
                        return canvas.toDataURL("image/png")
                    }).then(url => {
                        const preparedBlock = {
                            b64: url,
                            name: item.dataset.fileName
                        }
                        
                        files.push(preparedBlock)

                        if(files.length === targets.length) {
                            if (this.$root.debug) {
                                console.log('fetchScript')
                            }
                            this.fetchZipMultipleScript(files)
                        }
                    })
                })
            }, 100);
        },
        fetchZipMultipleScript: function(files) {
            const init = {
                method: 'POST',
                body: JSON.stringify(files)
            }
            
            fetch(this.scriptZipMultipleUrl, init).then(res => {
                return res.text()
            }).then(id => {
                this.downloadFiles(id)
            })
        },
        downloadFiles: function(id) {
            const a = document.createElement('a')
            a.download = 'TMNlab.zip'
            a.href = './assets/functions/TMNlab_' + id + '/TMNlab.zip'
            document.body.appendChild(a)
            
            a.click()
            document.body.removeChild(a)
            this.deleteFiles(id)
            this.inProgress = false
        },
        deleteFiles: function(id) {
            const init = {
                method: 'POST',
                body: id
            }
            fetch(this.scriptTmpDeleteUrl, init).then(res => {
                return res.text()
            }).then(data => {
                
            })
        }
    }
}

export default PanelRight