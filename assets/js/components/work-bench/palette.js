import EventBus from "../../eventBus.js";
import paletteMixin from "./palette-mixin.js";

const Palette = {
    mixins: [paletteMixin],
    template:`
        <section class="py-2 border-b">
            <h3 class="panel__item | font-bold">Palette</h3>
            <!-- <div class="panel__item panel__select">
                <label for="palette" class="sr-only">Gamme colorée</label>
                <select name="palette" id="palette" class="btn btn--select">
                    <option value="colors">Couleurs</option>
                    <option value="grayscale">Niveaux de gris</option>
                </select> 
            </div> -->
            <div class="panel__item panel__item--color">
                <label for="colors">Nuancier</label>
                <div class="flex pt-2">
                    <input v-for="(color, index) in colors" :key="index" type="color" :id="'color.normal' + index" :name="'color.normal' + index" v-model="color.normal" @change="changeColor(index, color.normal)">
                </div>
            </div>
        </section>
    `,
    methods: {
        changeColor: function(index, color) {
            const colorIndex = String(index++)

            const payload = {
                index: colorIndex,
                color: color
            }

            EventBus.$emit('change-color', payload)
        }
    }
}

export default Palette