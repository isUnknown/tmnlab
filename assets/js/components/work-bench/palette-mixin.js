import EventBus from "../../eventBus.js";

const paletteMixin = {
    data: function() {
        return {
            colors: {
                1: {
                    dark: null,
                    normal: getComputedStyle(document.documentElement).getPropertyValue('--color-brand-1'),
                    light: null
                },
                2: {
                    dark: null,
                    normal: getComputedStyle(document.documentElement).getPropertyValue('--color-brand-2'),
                    light: null
                },
                3: {
                    dark: getComputedStyle(document.documentElement).getPropertyValue('--color-brand-3-dark'),
                    normal: getComputedStyle(document.documentElement).getPropertyValue('--color-brand-3'),
                    light: getComputedStyle(document.documentElement).getPropertyValue('--color-brand-3-light')
                },
                4: {
                    dark: getComputedStyle(document.documentElement).getPropertyValue('--color-brand-4-dark'),
                    normal: getComputedStyle(document.documentElement).getPropertyValue('--color-brand-4'),
                    light: getComputedStyle(document.documentElement).getPropertyValue('--color-brand-4-light')
                }
            }
        }
    },
    methods: {
        generateVariants: function() {
            Object.keys(this.colors).forEach(key => {
                const normal = tinycolor(this.colors[key].normal)
                const light = normal.lighten(25).toString()
                const dark = normal.darken(25).toString()

                this.colors[key].light = light
                this.colors[key].dark = dark
            })
        },
        loadCustomColors: function() {
            if (localStorage.getItem('colors')) {
                this.colors = JSON.parse(localStorage.getItem('colors'))
            }
        }
    },
    created: function() {
        this.loadCustomColors()
        this.generateVariants()
        
        EventBus.$on('change-color', payload => {
            this.colors[payload.index].normal = payload.color
            // localStorage.setItem('colors', JSON.stringify(this.colors))
        })
    },
    beforeDestroy: function() {
        EventBus.$off('change-color')
    }
}

export default paletteMixin