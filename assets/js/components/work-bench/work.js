import EventBus from "../../eventBus.js"
import BlockCallout from "../blocks/block-callout.js"
import BlockGraph from "../blocks/block-graph.js"
import BlockQuote from "../blocks/block-quote.js"
import BlockSpec from "../blocks/block-spec.js"
import BlockTable from "../blocks/block-table.js"
import BlockText from "../blocks/block-text.js"
import BlockVs from "../blocks/block-vs.js"
import Part from "../part.js"
import Subpart from "../subpart.js"
import paletteMixin from "./palette-mixin.js"

const Work = {
    mixins: [paletteMixin],
    components: {
        'part': Part,
        'subpart': Subpart,
        'block-text': BlockText,
        'block-graph': BlockGraph,
        'block-callout': BlockCallout,
        'block-table': BlockTable,
        'block-quote': BlockQuote,
        'block-spec': BlockSpec,
        'block-vs': BlockVs
    },
    data: function() {
        return {
            work: null
        }
    },
    watch: {
        colors: {
            handler() {
                this.generateVariants()
            },
            deep: true
        }
    },
    template: `
        <div id="dialog-export-content">
            <!--
            <div class="work study | mx-auto py-6 px-6" :style="
                '--color-brand-1-dark:' + colors[1].dark + ';' +
                '--color-brand-1:' + colors[1].normal + ';' +
                '--color-brand-1-light:' + colors[1].light + ';' +

                '--color-brand-2-dark:' + colors[2].dark + ';' +
                '--color-brand-2:' + colors[2].normal + ';' +
                '--color-brand-2-light:' + colors[2].light + ';' +

                '--color-brand-3-dark:' + colors[3].dark + ';' +
                '--color-brand-3:' + colors[3].normal + ';' +
                '--color-brand-3-light:' + colors[3].light + ';' +

                '--color-brand-4-dark:' + colors[4].dark + ';' +
                '--color-brand-4:' + colors[4].normal + ';' +
                '--color-brand-4-light:' + colors[4].light + ';'
            ">
            -->
            <div class="work study | mx-auto py-6 px-6">
                <div class="work__item" v-for="item in work" :key="item.title" :data-file-name="item.fileName" v-if="item.isVisible">
                    <part v-if="item.type === 'part' || item.type === 'annex'" :part="item"></part>

                    <subpart v-if="item.type === 'subpart'" :subpart="item"></subpart>
                    
                    <component v-if="item.type !== 'annex' && item.type !== 'part' && item.type !== 'subpart'" :is="'block-' + item.type" :block="item"></component>
                </div>
            </div>
        </div>
    `,
    created: function() {
        EventBus.$on('set-work', work => {
            this.work = work
        })
        EventBus.$on('update-work', work => {
            this.work = work
        })
    },
    beforeDestroy: function() {
        EventBus.$off('set-work')
        EventBus.$off('update-work')
    }
}

export default Work