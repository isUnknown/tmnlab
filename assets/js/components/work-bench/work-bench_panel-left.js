import EventBus from "../../eventBus.js";

const PanelLeft = {
    props: {
        collection: Array
    },
    data: function() {
        return {
            customCollection: [],
            sortOption: 'collection'
        }
    },
    computed: {
        sortedCollection: function() {
            if (this.customCollection.length > 0) {
                let sortedCollection = this.customCollection.sort((prev, next) => {
                    return prev.index[this.sortOption] - next.index[this.sortOption]
                })
                return sortedCollection
            } else {
                return false
            }
        }
    },
    watch: {
        sortedCollection: function() {
            this.updateWork()
        }
    },
    template: `
    <div class="panel panel--left | border-r print:hidden">
        <section class="py-2 border-b">
            <h3 class="panel__item | font-bold">Ma collecte</h3>
            <div class="panel__item panel__select">
            <label for="order" class="sr-only">Ordre</label>
            <select name="order" id="order" class="btn btn--select" v-model="sortOption">
                <option value="collection">Ordre de ma collecte</option>
                <option value="absolute">Ordre de l’étude</option>
                <option value="custom">Ordre personnalisé</option>
            </select> 
            </div>
            
            <ul class="panel__list panel__list--blocks pt-2 mt-2 border-t">
                <li v-for="item in sortedCollection" :key="item.title" :class="{'opacity-40': !item.isVisible}">
                    <svg class="icon">
                        <use :xlink:href="'#icon-' + setIcon(item.type)" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                    
                    <span>{{ item.title }}</span>

                    <!-- UP & DOWN BTNS -->
                    <button 
                        v-if="sortOption === 'custom'"
                        @click="descend(item)"
                        class="btn--left btn--down"
                        title="Descendre"
                        :disabled="isLast(item) ? 'disabled': false"
                    >
                        <svg class="icon">
                            <use xlink:href="#icon-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                        <span class="sr-only">Down</span>
                    </button>
                    <button 
                        v-if="sortOption === 'custom'"
                        @click="ascend(item)" 
                        class="btn--left btn--up" 
                        title="Monter" 
                        :disabled="isFirst(item) ? 'disabled': false"
                    >
                        <svg class="icon">
                            <use xlink:href="#icon-up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                        <span class="sr-only">Monter</span>
                    </button>
                    <!-- / UP & DOWN BTNS -->
                    
                    <button class="btn--right" :title="item.isVisible ? 'Masquer' : 'Afficher'" @click="toggleVisibility(item)">
                        <svg class="icon">
                            <use :xlink:href="item.isVisible ? '#icon-visible' : '#icon-invisible'" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                        <span class="sr-only">Masquer</span>
                    </button>
                </li>
            </ul>
        </section>
    </div>
    `,
    methods: {
        isFirst: function(item) {
            return item.index.custom === 1
        },
        isLast: function(item) {
            return item.index.custom === this.customCollection.length
        },
        updateWork: function() {
            EventBus.$emit('update-work', this.sortedCollection)
        },
        toggleVisibility: function(item) {
            item.isVisible = !item.isVisible
            this.updateWork()
        },
        setCustomCollection: function() {
            let customCollection = this.collection.slice()

            customCollection.forEach(item => {
                item.index.custom = item.index.collection
            })

            this.customCollection = customCollection
        },
        setIcon: function(type) {
            if (type === 'spec' || type === 'vs') {
                return 'callout'
            } else if (type === 'annex') {
                return 'part'
            } else {
                return type
            }
        },
        ascend: function(item) {
            const prevItem = this.customCollection.find(el => el.index.custom === item.index.custom - 1)
            if (prevItem) {
                prevItem.index.custom++
                item.index.custom--

                // NECESSARY TO UPDATE THE COMPUTED RELATED TO SORTOPTION DATA
                if (this.sortOption === 'custom') {
                    this.sortOption = ''
                    this.sortOption = 'custom'
                }
            }
        },
        descend: function(item) {
            const nextItem = this.customCollection.find(el => el.index.custom === item.index.custom + 1)
            if (nextItem) {
                nextItem.index.custom--
                item.index.custom++

                // NECESSARY TO UPDATE THE COMPUTED RELATED TO SORTOPTION DATA
                if (this.sortOption === 'custom') {
                    this.sortOption = ''
                    this.sortOption = 'custom'
                }
            }
        }
    },
    created: function() {
        this.setCustomCollection()
    },
    mounted: function() {
        EventBus.$emit('set-work', this.customCollection)
    }
}

export default PanelLeft