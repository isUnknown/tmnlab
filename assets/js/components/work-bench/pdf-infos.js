const PDFInfos = {
  template: `
    <dialog
        id="dialog-pdf"
        role="dialog"
        aria-labelledby="dialog-label"
        aria-describedby="dialog-description"
        aria-modal="true"
        class="dialog"
        open
    >
        <header class="dialog__header | border-b print:hidden">
            
            <h2 id="dialog-label">Export PDF</h2>
            
            <button @click="close"
                type="button" 
                id="btn-pdf-close"
                value="close"
                class="btn btn--square btn--transparent"
                title="Fermer"
            >
                <svg class="icon"><use xlink:href="#icon-close" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg>
                <span class="sr-only">Fermer</span>
            </button>
        </header>

        <div class="dialog__body">
            <h3 class="panel__item | font-bold">Instructions</h3>
            <p class="panel__item">
                L’export PDF est généré via la fenêtre d’impression de votre navigateur. Pour un export optimal, veuillez à bien paramétrer les options comme indiqué ci-dessous.
            </p>
            <hr>
            <div class="firefox-only chrome-only edge-only opera-only">
                <div class="panel__item">
                    <label class="panel__label">
                        <span class="firefox-only chrome-only opera-only">Destination</span>
                        <span class="edge-only">Imprimante</span>
                     </label>
                    <select class="firefox-only chrome-only" name="destination">
                        <option value="save-as-pdf">Enregistrer au format PDF</option>
                    </select>
                    <select class="edge-only" name="destination">
                        <option value="save-as-pdf">Enregistre au format PDF</option>
                    </select>
                    <select class="opera-only" name="destination">
                        <option value="save-as-pdf">Enregistrer en PDF</option>
                    </select>
                </div>
            </div>
            <div class="firefox-only chrome-only edge-only opera-only">
                <hr>
            </div>
            <details open>
                <summary class="chrome-only">Plus de paramètres</summary>
                <summary class="edge-only">Paramètres supplémentaires</summary>
                <summary class="opera-only">Plus d’options</summary>
                <div class="firefox-only chrome-only">
                    <div class="panel__item">
                        <p class="panel__label">
                            <span class="firefox-only">Échelle</span>
                            <label class="chrome-only" for="scale-chrome">Mise à l’échelle</label>
                        </p>
                        <input class="firefox-only" type="radio" disabled name="scale-firefox" value="ajust" checked> <label class="firefox-only" for="scale-firefox">Ajuster à la largeur de la page</label><br class="firefox-only">
                        <input class="firefox-only" type="radio" disabled name="scale-firefox" value="scale"> <label class="firefox-only" for="scale-firefox">Échelle</label>
                        <select class="chrome-only" name="scale-chrome">
                            <option value="default">Par défaut</option>
                        </select>
                    </div>
                </div>
                <div class="firefox-only chrome-only edge-only opera-only">
                    <div class="panel__item">
                        <label class="panel__label" for="margins">Marges</label>
                        <select name="margins">
                            <option value="default">Par défaut</option>
                        </select>
                    </div>
                </div>
                <div class="panel__item">
                    <div class="firefox-only chrome-only edge-only opera-only">
                        <p class="panel__label">Options</p>
                    </div>
                    <select class="safari-only" name="safari">
                        <option value="safari">Safari</option>
                    </select><br class="safari-only"><br class="safari-only">

                    <input class="safari-only" type="checkbox" disabled checked name="options" value="print-background">
                    <label class="safari-only" for="options">Imprimer l’arrière-plan</label><br class="safari-only">
                    
                    <input type="checkbox" disabled name="options" value="print-header-footer"> 
                    <label for="options">
                        <span class="firefox-only safari-only">Imprimer les en-têtes et pieds de page</span>
                        <span class="chrome-only edge-only opera-only">En-têtes et pieds de page</span>
                    </label><br>

                    <input class="firefox-only chrome-only edge-only opera-only" type="checkbox" disabled checked name="options" value="print-background"> 
                    <label class="firefox-only chrome-only edge-only opera-only" for="options">
                        <span class="firefox-only">Imprimer les arrière-plans</span>
                        <span class="chrome-only">Graphiques d’arrière-plan</span>
                        <span class="edge-only">Graphisme de l’arrière-plan</span>
                        <span class="opera-only">Arrière-plan</span>
                    </label>
                </div>
            </details>
            <div class="safari-only">
                <hr>
                <div class="panel__item">
                    <p class="panel__label">Puis sélectionnez en bas gauche de la fenêtre</p>
                    <select name="pdf" style="display:inline-block">
                        <option value="pdf">PDF</option>
                    </select>
                    → Enregistrer au format PDF
                </div>
            </div>
        </div>

        <footer class="panel__item">
            <button 
                id="btn-pdf-export"
                class="btn btn--fill"
                @click="exportPDF"
            >
                Paramètres d’export PDF…
            </button>
        </footer>
    </dialog>
    `,
  methods: {
    close: function () {
      this.$emit("close");
      if (this.$root.debug) {
        console.log(this.$options._componentTag + " close");
      }
    },
    exportPDF: function () {
      window.print();
    },
    fileSrc: function () {
      const browser = this.getBrowser();
      const fileSrc = `assets/images/${browser}.png`;
      return fileSrc;
    },
    getBrowser: function () {
      let browser;

      if (/firefox/i.test(navigator.userAgent)) {
        browser = "firefox";
      } else if (/chrome/i.test(navigator.userAgent)) {
        browser = "chrome";
      } else if (/safari/i.test(navigator.userAgent)) {
        browser = "safari";
      } else if (/edge/i.test(navigator.userAgent)) {
        browser = "edge";
      }

      return browser;
    },
  },
};

export default PDFInfos;
