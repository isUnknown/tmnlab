import EventBus from "../../eventBus.js"
import PanelLeft from "./work-bench_panel-left.js"
import PanelRight from "./work-bench_panel-right.js"
import Work from "./work.js"

const WorkBench = {
    props: {
        scriptZipMultipleUrl: String,
        scriptTmpDeleteUrl: String
    },
    components: {
        'panel-left': PanelLeft,
        'work': Work,
        'panel-right': PanelRight
    },
    data: function() {
        return {
            isOpen: false,
            collection: []
        }
    },
    template: `
    <div class="work-bench" v-if="isOpen">
        <dialog id="dialog-export" role="dialog" aria-labelledby="dialog-label" aria-describedby="dialog-description" aria-modal="true" class="dialog" open>
            <header class="dialog__header | border-b print:hidden">
                <h2 id="dialog-label">(Re)travailler ma collecte</h2>
                <button type="button" value="close" class="btn btn--square btn--transparent" title="Fermer (Esc)" @click="close">
                <svg class="icon"><use xlink:href="#icon-close" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg>
                </button>
            </header>
            
            <panel-left
                :collection="collection"
            ></panel-left>
            
            <work></work>
            
            <panel-right
                :script-zip-multiple-url="scriptZipMultipleUrl"
                :script-tmp-delete-url="scriptTmpDeleteUrl"
                :collection="collection"
            ></panel-right>
        </dialog>
        <div class="dialog-overlay | print:hidden"></div>
    </div>
    `,
    methods: {
        close: function() {
            this.isOpen = false
            if (this.$root.debug) {
                console.log(this.$options._componentTag + ' close')
            }
        },
        indexSort: function() {
            this.collection
        }
    },
    created: function() {
        EventBus.$on('open-work-bench', (collection) => {
            this.collection = collection
            this.isOpen = true
        })
        document.addEventListener('keyup', key => {
            if (key.code === 'Escape') { this.isOpen = false}
        })
    },
    beforeDestroy: function() {
        EventBus.$off('open-work-bench')
        document.removeEventListener('keyup')
    }
}

export default WorkBench