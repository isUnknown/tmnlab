import Collection from "./collection.js"
import Ressources from "./ressources.js"

const PanelRight = {
    props: {
        isExpand: Boolean,
        currentElement: Object
    },
    components: {
        'collection': Collection,
        'ressources': Ressources
    },
    template: `
    <div class="panel panel--right print:hidden | border-l print:hidden">
        <collection>
        </collection>
        <ressources 
          :is-expand="isExpand"
          :current-element="currentElement"
          @expand="expand">
        </ressources>
    </div>
    `,
    methods: {
        expand: function() {
            this.$emit("expand")
        }
    }
}

export default PanelRight