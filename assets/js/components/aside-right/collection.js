import EventBus from "../../eventBus.js"
import iconMixin from "../icon-mixin.js"

const Collection = {
    mixins: [iconMixin],
    data: function() {
        return {
            items: []
        }
    },
    computed: {
        isEmpty: function() {
            return this.items.length === 0
        }
    },
    template: `
        <section class="collection | py-2">
            <h2 class="panel__item | font-bold">Ma collecte</h2>
            <p class="panel__item | opacity-50" v-if="isEmpty">Aucun contenu selectionné</p>
            <ul class="panel__list panel__list--blocks">
                <li v-for="item in items" :key="item.slug">
                    <svg class="icon">
                        <use :xlink:href="'#icon-' + setIcon(item.type)" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                    <a :href="'#' + item.index.absolute"><span>{{ item.title }}</span></a>
                    <button class="btn--close" title="Supprimer" @click="remove(item)">
                        <svg class="icon">
                            <use xlink:href="#icon-close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                        <span class="sr-only">Supprimer</span>
                    </button>
                </li>
            </ul>
            <div class="panel__item">
                <button class="btn btn--fill" @click="openWorkBench" :disabled="isEmpty ? true : false">(Re)travailler ma collecte</button>
            </div>
        </section>
    `,
    methods: {
        add: function(newItem) {
            if (this.items.length === 0) {
                this.items.push(newItem)
                if (this.$root.debug) {
                    console.log('Collection.add(): ', newItem)
                }
            } else {
                if (this.hasParent(newItem)) {
                    if (this.$root.debug) {
                        console.log('Collection.add(): parent already exists')
                    }
                } else {
                    this.addIfNew(newItem)
                }
            }
        },
        hasParent: function(newItem) {
            if (newItem.level === 'block') {
                return this.items.some(collectedItem => (collectedItem.index.absolute === newItem.index.parent) || (collectedItem.index.absolute === newItem.index.grandParent))
            } else if (newItem.level === 'subpart') {
                return this.items.some(collectedItem => ((collectedItem.index.absolute === newItem.index.parent)))
            }
        },
        addIfNew: function(newItem) {
            if (!this.items.some(item => item.index.absolute === newItem.index.absolute)) {
                this.items.push(newItem)
                this.deleteChildren(newItem)
                if (this.$root.debug) {
                    console.log('Collection.add(): ', newItem)
                }
            } else {
                if (this.$root.debug) {
                    console.log('Collection.addIfNew(): item already exists')
                }
            }
        },
        deleteChildren: function(newItem) {
            let newCollection = this.items
            if (newItem.level === 'part') {
                newItem.subparts.forEach(subpart => {
                    newCollection = newCollection.filter(collectedItem => collectedItem.index.absolute !== subpart.index.absolute)
                    subpart.blocks.forEach(block => {
                        newCollection = newCollection.filter(collectedItem => collectedItem.index.absolute !== block.index.absolute)
                    })
                })
            } else if (newItem.level === 'subpart') {
                newItem.blocks.forEach(block => {
                    newCollection = newCollection.filter(collectedItem => collectedItem.index.absolute !== block.index.absolute)
                })
            }
            this.items = newCollection
        },
        remove: function(target) {
            target.index.collection = false
            this.items = this.items.filter(item => item !== target)

            if (this.$root.debug)
                console.log('Collection.remove(): ', target)
        },
        updateCollectionIndex: function() {
            this.items.forEach(item => {
                if(this.items.indexOf(item) !== -1) {
                    item.index.collection = this.items.indexOf(item) + 1
                }
            });
        },
        openWorkBench: function() {
            EventBus.$emit('open-work-bench', this.items)
        },
        saveCollection: function() {
            localStorage.setItem('collection', JSON.stringify(this.items))
        },
        loadCollection: function() {
            if (localStorage.getItem('collection')) {
                this.items = JSON.parse(localStorage.getItem('collection'))
            }
        }
    },
    created: function() {
        EventBus.$on('collect', item => {
            this.add(item)
        })
        this.loadCollection()
    },
    updated: function() {
        this.updateCollectionIndex()
        this.saveCollection()
    }
}

export default Collection