import EventBus from '../../eventBus.js'
import submitRessource from './submit-ressource.js'

const Ressources = {
    props: {
      isExpand: Boolean,
      currentElement: Object
    },
    components: {
      'submit-ressource': submitRessource
    },
    data: function() {
        return {
            ressources: [],
            isOpenSubmit: false
        }
    },
    computed: {
      partsNotAnnex: function() {
        return this.$root.parts.filter(part => part.type !== 'annex')
      },
      currentPart: function() {
        if (!this.currentElement.index) return false

        if (this.currentElement.type === 'part') {
          return this.currentElement
        } else {
          return this.partsNotAnnex.find(part => part.index.roman === this.currentElement.index.romanPartParent)
        } 
      }
    },
    template: `
        <section id="ressources" class="py-2 border-t" :class="{ 'expanded': isExpand }">
            <header :class="{ 'pb-2 border-b': isExpand }">
                <h2 class="panel__item | font-bold">Ressources</h2>
                <button 
                  type="button" 
                  id="btn-ressource-expand" 
                  value="expand" 
                  :title="isExpand ? 'Réduire' : 'Agrandir'" 
                  class="btn btn--square btn--transparent"
                  :aria-expanded="isExpand ? 'true' : 'false'"
                  aria-controls="ressources"
                  @click="expand"
                >
                  <svg class="icon"><use xlink:href="#icon-expand" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg> <span class="sr-only">{{ isExpand ? 'Réduire' : 'Agrandir' }}</span>
                </button>
            </header>
            <ul v-if="!isExpand" class="panel__list panel__list--ressources">
              <li class="ressource" v-for="ressource in ressources">
                <p class="ressource__meta">
                  <span v-if="ressource.type" class="ressource__type">{{ ressource.type }}</span>
                  <span class="ressource__source">{{ ressource.rootUrl }}</span>
                  <time v-if="ressource.published" :datetime="ressource.date">{{ ressource.published }}</time>
                </p>
                <p><a :href="ressource.link" target="_blank" rel="noopener">{{ ressource.name }}</a></p>
              </li>
            </ul>
            
            <details v-else v-for="part in partsNotAnnex" :open="currentPart && part.index.roman === currentPart.index.roman ? true: false">
              <summary class="panel__item border-b">
                {{ part.index.roman }}.
                <span>{{ part.title }}</span>
                <svg class="icon"><use xlink:href="#icon-carret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg>
              </summary>
              <ul>
                <li class="border-b" v-for="subpart in part.subparts">
                  <details :open="(currentElement.type === 'subpart' || currentElement.type === 'heading') && (currentElement.index.absolute === subpart.index.absolute || currentElement.index.subpart === subpart.index.inParent) ? true: false">
                    <summary class="panel__item">
                      {{ subpart.index.inParent }}.
                      <span>{{ subpart.title }}</span>
                      <svg class="icon"><use xlink:href="#icon-carret-down" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg>
                    </summary>
                    <ul class="panel__list panel__list--ressources">
                      <li class="ressource" v-for="ressource in subpart.ressources">
                        <p class="ressource__meta">
                          <span v-if="ressource.type" class="ressource__type">{{ ressource.type }}</span> 
                          <span class="ressource__source">{{ ressource.rootUrl }}</span>
                          <time v-if="ressource.published" :datetime="ressource.date">{{ ressource.published }}</time>
                        </p>
                        <p><a :href="ressource.link">{{ ressource.name }}</a></p>
                      </li>
                    </ul>
                  </details>
                </li>
              </ul>
            </details>
            <div v-if="isExpand" class="panel__item ctas">
              <a class="btn btn--outline" download href="assets/csv/ressources.csv">Télécharger les ressources (CSV)</a>
            </div>
        </section>
    `,
    methods: {
        add: function(ressources) {
            this.ressources = ressources
            if (this.$root.debug) {
                console.log('Ressource.add(): ', ressources)
            }
        },
        expand: function() {
          this.$emit('expand')
        }
    },
    created: function() {
        EventBus.$on('change-ressources', ressources => {
            this.add(ressources)
        })
    }
}

export default Ressources