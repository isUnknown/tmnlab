import EventBus from "../eventBus.js"
import iconMixin from "./icon-mixin.js"

const Note = {
    mixins: [iconMixin],
    data: function() {
        return {
            isOpen: false,
            currentIndex: null,
            notes: []
        }
    },
    computed: {
        currentNote: function() {
            const note = this.notes.find(note => note.index === this.currentIndex)
            if (note) {
                return note
            } else {
                return false
            }
        }
    },
    template: `
        <dialog v-if="isOpen" id="dialog-export" role="dialog" aria-labelledby="dialog-label" aria-describedby="dialog-description" aria-modal="true" class="dialog" open>
            <input type="textarea" v-model="currentNote.text" />
        </dialog>
    `,
    methods: {
        openDialog: function() {
            this.isOpen = true
            if (this.$root.debug) {
                console.log(this.$options._componentTag + ' open: ', )
            }
        },
        openNote: function(index) {
            this.currentIndex = index
            if (!this.currentNote) {
                this.notes.push({
                    'index': index,
                    'text': ''
                })
            }
        },
        loadNotes: function() {
            this.notes = JSON.parse(localStorage['notes'])
        }
    },
    created: function() {
        EventBus.$on('open-note', (index) => {
            this.openDialog()
            this.openNote(index)
        })

        if (localStorage['notes']) {
            this.loadNotes()
        }
    },
    beforeDestroy: function() {
        Event.$off('open-note')
    }
}

export default Note