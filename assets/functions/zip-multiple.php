<?php

$json = file_get_contents("php://input");
$data = json_decode($json);

$id = uniqid();
$folder = 'TMNlab_' . $id . '/';

mkdir($folder, 0777, true);

$files = [];
foreach ($data as $item) {
    $b64 = $item->b64;
    $path = './' . $folder . $item->name . '.png';
    $files[] = $path;
    $file = fopen($path, "wb");

    $data = explode(',', $b64);

    fwrite($file, base64_decode($data[1]));
    fclose($file);
}

$zipname = $folder . 'TMNlab.zip';
$zip = new ZipArchive;
$zip->open($zipname, ZipArchive::CREATE);

foreach ($files as $key => $file) {
  $zip->addFile($file);
  if ($key === sizeof($files) - 1) {
    echo $id;
  }
}

$zip->close();