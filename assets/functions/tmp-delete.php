<?php

function removeDirectory($path) {

  if (is_dir($path)) {
    $files = glob($path . '/*');
    foreach ($files as $file) {
      is_dir($file) ? removeDirectory($file) : unlink($file);
    }
    rmdir($path);

    return;
  }
}

$id = file_get_contents("php://input");
$dir = './TMNlab_' . $id;

sleep(30);
removeDirectory($dir);