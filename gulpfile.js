const { src, dest } = require('gulp')
const cleanCSS = require('gulp-clean-css')
const autoprefixer = require('gulp-autoprefixer')

exports.default = function() {
  return src('assets/css/style.css')
    .pipe(autoprefixer({
			cascade: false
		}))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(dest('assets/dist'))
}