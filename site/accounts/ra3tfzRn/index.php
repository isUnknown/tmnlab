<?php

return [
    'email' => 'alixturcq@collectifbam.fr',
    'language' => 'fr',
    'name' => 'Alix',
    'role' => 'admin'
];