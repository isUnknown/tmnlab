<?php

return [
    'email' => 'morgane@collectifbam.fr',
    'language' => 'fr',
    'name' => 'Morgane',
    'role' => 'admin'
];