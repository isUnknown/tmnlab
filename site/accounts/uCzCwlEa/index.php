<?php

return [
    'email' => 'clement.coustenoble@tmnlab.com',
    'language' => 'fr',
    'name' => 'Clément',
    'role' => 'admin'
];