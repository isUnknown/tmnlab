<?php snippet('header') ?>
    <main id="home" role="main">        
        <section class="container | px-8">
            <div class="study">
                <header class="block--title">
                    <h1><?= $page->title() ?></h1>
                    <?php if ($page->introduction()->isNotEmpty()): ?>
                    <p><?= $page->introduction() ?></p>
                    <?php endif ?>
                </header>
                <?php $etudes = $site->children()->listed() ?>
                <ul class="etudes | border-b">
                    <?php foreach ($etudes as $etude): ?>
                    <li class="etude | border-t">
                        <article>
                            <?php if ($etude->date()->isNotEmpty()): ?>
                            <time class="mono text-gray-500" datetime="<?= $etude->date() ?>"><?= $etude->date()->toDate('d/m/Y') ?></time>
                            <?php endif ?>
                            <h2><a href="<?php e($etude->external()->toBool(), $etude->href(), $etude->url()) ?>"><?= $etude->title() ?></a></h2>
                            <?php if ($etude->description()->isNotEmpty()): ?>
                            <p><?= $etude->description() ?></p>
                            <?php endif ?>
                        </article>
                    </li>
                    <?php endforeach ?>
                </ul>
                <component :is="'block-' + block.type" v-for="block in blocks" :block="block" :key="block.title" :is-home="true"></component>
            </div>
        </section>
    </main>
<?php snippet('footer') ?>