<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $site->title() ?> - <?= $page->title() ?></title>
    <link rel="stylesheet" href="<?= url('assets') ?>/style.css">
    
    <!--========== VUE ==========-->
    <!-- development version, includes helpful console warnings -->
    <script src="<?= url('assets') ?>/js/libs/vue.js"></script>
    <script src="<?= url('assets') ?>/js/app.js" type="module" defer></script>
</head>

<body>
    <div id="app" :class="{ cursor_selection: tools.isSelection }">
        <interactive-scrollbar></interactive-scrollbar>
        
        <etabli></etabli>
        
        <?php foreach($page->builder()->toBlocks() as $block): ?>
            <?= $block ?>
        <?php endforeach ?>
    </div>
</body>

</html>