<?php snippet('header') ?>
  <main id="app" role="main" data-page="<?= $page->title() ?>" :data-mobile-banner="isMobile ? 'true' : 'false'" :class="isPanelRightExpand ? 'panel-right-expanded' : false" :style="isPanelRightExpand ? '--panel-right-expanded-w: 32rem' : false">

    <!-- HOME -->
    <study-intro>
      <div class="intro__blocks opacity-0">
        <?php foreach ($page->builder()->toBlocks() as $block): ?>
        <div class="block block--<?= $block->type() ?>">
          <?= $block ?>
        </div>
        <?php endforeach ?>
      </div>
    </study-intro>

    <template>
      <tutorial></tutorial>

      <!-- TOGGLE TOC -->
      <input type="checkbox" id="toc" name="toggle-toc" class="sr-only print:hidden">
      <label id="toggle-toc" for="toc" class="btn btn--item btn--circle print:hidden" title="Sommaire">
          <span class="sr-only">Sommaire</span>
          <svg class="icon"><use id="toggle-icon-toc" xlink:href="#icon-toc" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg>
      </label>
      
      <panel-left
        :flat-index="flatIndex"
      ></panel-left>
      
      <!-- TOGGLE QUESTIONS -->
      <input type="checkbox" id="questions" name="questions" class="sr-only print:hidden">
      <label id="toggle-questions" for="questions" class="btn btn--item btn--circle print:hidden" title="Questionnaire">
          <span class="sr-only">Questionnaire</span>
          <svg class="icon"><use id="toggle-icon-question" xlink:href="#icon-question" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg>
      </label>
      
      <Study
          :parts="parts"
      ></Study>
      
      <panel-right 
        :is-expand="isPanelRightExpand" 
        :current-element="currentElement"
        @expand="expandPanelRight"
      ></panel-right>

      <work-bench
        :script-zip-multiple-url="'<?= url('assets') ?>/functions/zip-multiple.php'"
        :script-tmp-delete-url="'<?= url('assets') ?>/functions/tmp-delete.php'"
      ></work-bench>

      <mobile-banner v-if="isMobile" @close-mobile-banner="closeMobileBanner"></mobile-banner>
    </template>
  </main>
<?php snippet('footer') ?>