<?php
function csv(string $file, string $delimiter = ','): array
{
    // Sauvegarde le contenu du fichier dans $lines
    $lines = file($file);

    // Supprime le premier caractère caché de la clé de la première colonne
    $lines[0] = str_replace("\xEF\xBB\xBF", '', $lines[0]);

    $csv = array_map(function($d) use($delimiter) {
        return str_getcsv($d, $delimiter);
    }, $lines);

    // array_walk($csv, function(&$a) use ($csv) {
    //    $a = array_combine($csv[0], $a);
    // });

    // Déplace le premier élément de $csv (qui contient les en-tête de colonnes) dans $header
    // $header = array_shift($csv);

    return $csv;
}

//================================================================ FUNCTIONS
function numberToRomanRepresentation($number) {
    $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
    $returnValue = '';
    while ($number > 0) {
        foreach ($map as $roman => $int) {
            if($number >= $int) {
                $number -= $int;
                $returnValue .= $roman;
                break;
            }
        }
    }
    return $returnValue;
}

function getRootUrl($url) {
  return str_replace('www.', '', parse_url($url)['host']);
}

function createSlug($text){

    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}

function prepareBlock($isPart = false, $preparedPart = false, $rawBlock, $partIndex = 1, $blockIndex, $absoluteIndex, $subpartIndex = false) {    
    //================================================================ SET CONTENT
    switch ($rawBlock->type()) {
        case 'image' : {
            $preparedBlock['title'] = $rawBlock->image()->toFile()->filename();
            $preparedBlock['src'] = $rawBlock->image()->toFile()->url();
            $preparedBlock['alt'] = $rawBlock->alt()->value();
            $preparedBlock['caption'] = $rawBlock->caption()->value();
            break;
        }
        case 'heading': {
            $preparedBlock['title'] = (string)Str::unhtml($rawBlock->text()->short(60));
            $preparedBlock['text'] = (string)$rawBlock->text();
            break;
        }
        case 'text': {
            $preparedBlock['title'] = (string)Str::unhtml($rawBlock->text()->short(60));
            $preparedBlock['html'] = (string)$rawBlock->text()->kt();
            break;
        }
        case 'quote': {
            $preparedBlock['title'] = (string)Str::unhtml($rawBlock->text()->short(60));
            $preparedBlock['html'] = Str::unhtml((string)$rawBlock->text());
            $preparedBlock['author'] = Str::unhtml((string)$rawBlock->citation());
            break;
        }
        case 'graph': {
            $preparedBlock['title'] = (string)Str::unhtml($rawBlock->name());
            $preparedBlock['variant'] = $rawBlock->questiontype()->value();
            if ($rawBlock->csvFile()->isNotEmpty()) {
                $lines = csv($rawBlock->csvFile()->toFile());
                $preparedBlock['lines'] = $lines;
            }
            break;
        }
        case 'table': {
            $preparedBlock['title'] = (string)Str::unhtml($rawBlock->question());
            if ($rawBlock->table()->isNotEmpty()) {
                $lines = csv($rawBlock->table()->toFile());
                $preparedBlock['lines'] = $lines;
            }
            break;
        }
        case 'spec':
        case 'vs': 
            $preparedBlock['title'] = (string)Str::unhtml($rawBlock->text()->short(60));
            $preparedBlock['html'] = (string)$rawBlock->text()->kt();
            break;

        case 'callout': {
            $preparedBlock['title'] = Str::unhtml($rawBlock->builder()->toBlocks()->first()->text()->short(60)->value());
            $preparedBlock['blocks'] = [];
        
            foreach ($rawBlock->builder()->toBlocks() as $rawCalloutBlock) {
                $preparedCalloutBlock = [];
                $preparedCalloutBlock['type'] = (string)$rawCalloutBlock->type();
                $preparedCalloutBlock['title'] = 'Encadré_' . uniqid();
                $preparedCalloutBlock['calloutBlock'] = true;
                $preparedCalloutBlock['fileName'] = $partIndex . '.' . $blockIndex . '-' . createSlug($preparedBlock['title']);

                
                switch ($rawCalloutBlock->type()) {
                    case 'heading': {
                        $preparedCalloutBlock['text'] = Str::unhtml((string)$rawCalloutBlock->text());
                        break;
                    }
                    case 'text': {
                        $preparedCalloutBlock['html'] = (string)$rawCalloutBlock->text()->kt();
                        break;
                    }
                    case 'quote': {
                        $preparedCalloutBlock['html'] = Str::unhtml((string)$rawCalloutBlock->text());
                        $preparedCalloutBlock['author'] = Str::unhtml((string)$rawCalloutBlock->citation());
                        break;
                    }
                }
                
                $preparedBlock['blocks'][] = $preparedCalloutBlock;
            }
            break;
        }
    }

    //================================================================ SET GENERIC DATA

    if ($preparedPart) {
        $preparedBlock['fileName'] = $partIndex . '.' . $blockIndex . '-' . createSlug($preparedBlock['title']);
        $preparedBlock['type'] = $rawBlock->type();
        $preparedBlock['level'] = 'block';
        $preparedBlock['isVisible'] = true;
        $preparedBlock['index'] = [
            'isCurrent' => false,
            'collection' => false,
            'inParent' => $blockIndex,
            'absolute' => $absoluteIndex,
            'parent' => $preparedPart['index']['absolute'],
            'subpart' => $subpartIndex,
            'romanPartParent' => $isPart ? numberToRomanRepresentation($partIndex) : false
        ];
    } else {
        $preparedBlock['type'] = $rawBlock->type();
        $preparedBlock['level'] = 'block';
        $preparedBlock['isVisible'] = true;
    }
    $preparedBlock['comment'] = '';

    return $preparedBlock;
}

function prepareJsonStudy($study) {

    //================================================================ SET VERSION
    $newVersion = date('d-m-y_H-i-s');
    $newVersionExplode = explode('_', $newVersion);
    $newVersionDay = $newVersionExplode[0];
    $newVersionTime = $newVersionExplode[1];
    $newVersionTimeExplode = explode('-', $newVersionTime);
    $newVersionHour = $newVersionTimeExplode[0];
    $newVersionmMin = $newVersionTimeExplode[1];


    $previousFile = file_get_contents($study->contentFileDirectory() . '/data.json');
    if($previousFile) {
        $previousJson = json_decode($previousFile,true);
        if (array_key_exists('version', $previousJson)) {
            $previousUpdate = $previousJson['version'];
            $previousUpdateExplode = explode('_', $previousUpdate);
            $previousUpdateDay = $previousUpdateExplode[0];
            $previousUpdateTime = $previousUpdateExplode[1];
            $previousUpdateTimeExplode = explode('-', $previousUpdateTime);
            $previousUpdateHour = $previousUpdateTimeExplode[0];
            $previousUpdatemMin = $previousUpdateTimeExplode[1];
            
            $previousUpdateWasInLastMin = ($newVersionDay === $previousUpdateDay) && ($newVersionmMin === $previousUpdateHour) && ($newVersionmMin === $previousUpdatemMin);
            if ($previousUpdateWasInLastMin) {
                sleep(30);
            }
        }
    }
    
    $absoluteIndex = 0;
    
    //================================================================ PARTS
    $partIndex = 0;
    $preparedParts = [];
    $flatIndex = [];
    $flatRessources = [];
    $csvRessources = [];
    foreach ($study->children() as $rawPart) {

        $isPart = $rawPart->template()->name() === 'part';
        $isAnnex = $rawPart->template()->name() === 'annex';
        
        // INDEXES INCREMENTATION
        if ($isPart) {
            $partIndex++;
        }
        $absoluteIndex++;

        // PART DATA
        $preparedPart = [];
        $preparedPart['title'] = (string)$rawPart->title();
        $preparedPart['slug'] = (string)$rawPart->slug();
        $preparedPart['type'] = (string)$rawPart->template()->name();
        $preparedPart['level'] = 'part';
        $preparedPart['isVisible'] = true;
        $preparedPart['isAnnex'] = $isAnnex;
        $preparedPart['fileName'] = $partIndex . '-' . createSlug($preparedPart['title']);
        $preparedPart['index'] = [
            'isCurrent' => false,
            'collection' => false,
            'inParent' => $partIndex,
            'absolute' => $absoluteIndex,
            'roman' => $isPart ? numberToRomanRepresentation($partIndex) : false
        ];

        if ($isAnnex) {
            $blockIndex = 0;
            $blocks = [];
            foreach ($rawPart->builder()->toBlocks() as $rawBlock) {
                $blockIndex++; 
                $absoluteIndex++; 
                    
                $preparedBlock = prepareBlock($isPart, $preparedPart, $rawBlock, $partIndex, $blockIndex, $absoluteIndex);
                
                $blocks[] = $preparedBlock;
            }
            $preparedPart['blocks'] = $blocks;
        } else {
          $csvRessources[] = [$preparedPart['index']['roman'] . '. ' . $preparedPart['title']];
        }

        // FLAT INDEX
        $flatPart = [
            'title' => $preparedPart['title'],
            'index' => $preparedPart['index'],
            'type' => $preparedPart['type'],
            'isBookmarked' => false
        ];
        $flatIndex[] = $flatPart;

        //================================================================ SUBPARTS
        $subparts = [];
        $subpartIndex = 0;
        foreach ($rawPart->children() as $rawSubpart) {
            $subpartIndex++;
            $absoluteIndex++;
            
            // SUBPART DATA
            $preparedSubpart = [];
            $preparedSubpart['title'] = (string)$rawSubpart->title();
            $preparedSubpart['slug'] = (string)$rawSubpart->slug();
            $preparedSubpart['type'] = 'subpart';
            $preparedSubpart['level'] = 'subpart';
            $preparedSubpart['isVisible'] = true;
            $preparedSubpart['fileName'] = $partIndex . '.' . $subpartIndex . '-' . createSlug($preparedSubpart['title']);
            $preparedSubpart['index'] = [
                'isCurrent' => false,
                'collection' => false,
                'inParent' => $subpartIndex,
                'parent' => $preparedPart['index']['absolute'],
                'absolute' => $absoluteIndex,
                'romanPartParent' => numberToRomanRepresentation($partIndex),
                'isAnnex' => $isAnnex
            ];

            //================================================================ RESSOURCES
            $csvRessources[] = [$preparedSubpart['index']['inParent'] . '. ' . $preparedSubpart['title']];
            $ressources = [];
            foreach ($rawSubpart->ressources()->toStructure() as $rawRessource) {
                $preparedRessource = [
                    'name' => (string)$rawRessource->name(),
                    'link' => (string)$rawRessource->link(),
                    'rootUrl' => getRootUrl((string)$rawRessource->link()),
                    'type' => (string)$rawRessource->category(),
                    'published' => (string)$rawRessource->published()->toDate('d/m/Y'),
                    'date' => (string)$rawRessource->published()->toDate('Y-m-d')
                ];
                $ressources[] = $preparedRessource;
                $csvRessources[] = [
                  (string)$rawRessource->name(),
                  (string)$rawRessource->link()
                ];
            }
            $preparedSubpart['ressources'] = $ressources;

            $flatSubpart = [
                'title' => $preparedSubpart['title'],
                'index' => $preparedSubpart['index'],
                'type' => $preparedSubpart['type'],
                'ressources' => $preparedSubpart['ressources'],
                'isBookmarked' => false
            ];
            $flatIndex[] = $flatSubpart;

            //================================================================ BLOCKS
            $blockIndex = 0;
            $blockHeadingIndex = 0;
            $blocks = [];
            foreach ($rawSubpart->builder()->toBlocks() as $rawBlock) {
                $blockIndex++; 
                $absoluteIndex++; 

                $preparedBlock = prepareBlock($isPart, $preparedPart, $rawBlock, $partIndex, $blockIndex, $absoluteIndex, $subpartIndex);

                if ($preparedBlock['type'] === 'heading') {
                    $blockHeadingIndex++;
                    $preparedBlock['index']['heading'] = $blockHeadingIndex;
                    
                    $flatHeading = [
                        'title' => $preparedBlock['title'],
                        'index' => $preparedBlock['index'],
                        'type' => $preparedBlock['type'],
                        'isBookmarked' => false
                    ];
                    $flatIndex[] = $flatHeading;
                }

                $blocks[] = $preparedBlock;
            }
            
            

            $preparedSubpart['blocks'] = $blocks;
            $subparts[] = $preparedSubpart;
        }

        // PUSH
        $preparedPart['subparts'] = $subparts;
        $preparedParts[] = $preparedPart;
    }

    $preparedStudy = [
        'parts' => $preparedParts,
        'flatIndex' => $flatIndex,
        'version' => $newVersion
    ];

    $ressourcesFile = fopen('assets/csv/ressources.csv', 'w');
    foreach ($csvRessources as $ressource) {
        fputcsv($ressourcesFile, $ressource);
    }
    fclose($ressourcesFile);
    return json_encode($preparedStudy);
}

function prepareJsonHome($source) {    
    //================================================================ SET VERSION
    $newVersion = date('d-m-y_H-i-s');
    $newVersionExplode = explode('_', $newVersion);
    $newVersionDay = $newVersionExplode[0];
    $newVersionTime = $newVersionExplode[1];
    $newVersionTimeExplode = explode('-', $newVersionTime);
    $newVersionHour = $newVersionTimeExplode[0];
    $newVersionmMin = $newVersionTimeExplode[1];


    $previousFile = file_get_contents($source->contentFileDirectory() . '/data.json');
    if($previousFile) {
        $previousJson = json_decode($previousFile,true);
        if (array_key_exists('version', $previousJson)) {
            $previousUpdate = $previousJson['version'];
            $previousUpdateExplode = explode('_', $previousUpdate);
            $previousUpdateDay = $previousUpdateExplode[0];
            $previousUpdateTime = $previousUpdateExplode[1];
            $previousUpdateTimeExplode = explode('-', $previousUpdateTime);
            $previousUpdateHour = $previousUpdateTimeExplode[0];
            $previousUpdatemMin = $previousUpdateTimeExplode[1];
            
            $previousUpdateWasInLastMin = ($newVersionDay === $previousUpdateDay) && ($newVersionmMin === $previousUpdateHour) && ($newVersionmMin === $previousUpdatemMin);
            if ($previousUpdateWasInLastMin) {
                sleep(30);
            }
        }
    }

    //================================================================ BLOCKS
    $absoluteIndex = 0;
    $blockIndex = 0;
    $blocks = [];
    foreach ($source->builder()->toBlocks() as $rawBlock) {
        $blockIndex++; 
        $absoluteIndex++; 
        
        $preparedBlock = prepareBlock(null, null, $rawBlock, null, $blockIndex, $absoluteIndex);
        $blocks[] = $preparedBlock;
    }
    
    
    $preparedHome = [
        'blocks' => $blocks,
        'version' => $newVersion
    ];

    return json_encode($preparedHome);
}