<?php
Kirby::plugin('bam/multigroups-field', [
    'fields' => [
        'multigroups' => [
            'props' => [
                //etude
                'page' => function(string $page) {
                    return $page;
                },
                //filters
                'field' => function(string $field) {
                    return $field;
                }
            ],
            'computed' => [
                'test' => function() {
                    return $this->page . ' ' . $this->field;
                },
                'groups' => function() {
                    $site = site();
                    $pageName = $this->page;
                    $fieldName = $this->field;
                    $field = $site->index()->find($pageName)->$fieldName();
                    
                    $groups = [];
                    foreach ($field->toStructure() as $rawGroup) {

                        $preparedValues = [];
                        foreach ($rawGroup->values() as $value) {
                            $preparedValues[] = (string)$value;
                        }

                        $preparedGroup = [
                            'name' => (string)$rawGroup->name(),
                            'values' => $preparedValues
                        ];
                        $groups[] = $preparedGroup;
                    }
                    return $groups;
                }
            ]
        ]
    ]
]);