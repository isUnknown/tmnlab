panel.plugin('bam/multigroups-field', {
    fields: {
        multigroups: {
            props: {
                groups: Array
            },
            data: function() {
                return {
                    arr: ['A', 'B']
                }
            },
            template: `
                <div>
                    <p>Hello</p>
                    <p v-for="item in arr" :key="item">
                        {{ item }}
                    </p>
                    <p v-for="group in groups" :key="group.name">
                        {{ group.name }}
                    </p>
                </div>
            `
        }
    }
})