panel.plugin("tmnlab/vs-block", {
    blocks: {
      vs: {
        template: 
        `<div>
          <h3>VS. 2016 :</h3>
          <br />
          <k-writer
            ref="input"
            :inline="textField.inline"
            :marks="textField.marks"
            :nodes="textField.nodes"
            :placeholder="'Écrire ici…'"
            :value="content.text"
            class="k-block-type-vs-input"
            @input="update({ text: $event })"
          />
        </div>`,
        computed: {
          textField() {
            return this.field("text", {});
          }
        },
        methods: {
          focus() {
            this.$refs.input.focus();
          }
        }
      }
    }
  })