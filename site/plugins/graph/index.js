panel.plugin("tmnlab/graph-block", {
    blocks: {
      graph: `
        <div @click="open">
            <k-icon type="chart" style="display: inline-block; margin-right: .5rem;"></k-icon>
            {{ content.name }}
        </div>
      `
    }
  })