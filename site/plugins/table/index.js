panel.plugin("tmnlab/table-block", {
    blocks: {
      table: `
        <div @click="open">
            <k-icon type="bars" style="display: inline-block; margin-right: .5rem;"></k-icon>
            {{ content.question }}
        </div>
      `
    }
  })