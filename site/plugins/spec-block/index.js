panel.plugin("tmnlab/spec-block", {
    blocks: {
      spec: {
        template: 
        `<div>
          <h3>Spécificité à noter :</h3>
          <br />
          <k-writer
            ref="input"
            :inline="textField.inline"
            :marks="textField.marks"
            :nodes="textField.nodes"
            :placeholder="'Écrire ici…'"
            :value="content.text"
            class="k-block-type-spec-input"
            @input="update({ text: $event })"
          />
        </div>`,
        computed: {
          textField() {
            return this.field("text", {});
          }
        },
        methods: {
          focus() {
            this.$refs.input.focus();
          }
        }
      }
    }
  })