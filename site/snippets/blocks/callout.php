<div class="block block--callout">
  <?php foreach ($block->builder()->toBlocks() as $block): ?>
  <div class="block block--<?= $block->type() ?>">
    <?= $block ?>
  </div>
  <?php endforeach ?>
</div>
