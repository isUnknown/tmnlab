<figure class="block--quote">
  <blockquote>
    <p><?= $block->text() ?></p>
  </blockquote>
  <?php if ($block->citation()->isNotEmpty()): ?>
  <figcaption>
    <?= $block->citation() ?>
  </figcaption>
  <?php endif ?>
</figure>