<h1><?= $block->name() ?></h1>
<figure class="selectable" @click="select" data-type="simple" data-title="<?= $block->name() ?>">
    <div class="graph graph--square">
        <div class="font-data w-100 h-100 fl fade-out-siblings">
        <?php 
            $dataIndex = 1;
            foreach($block->data()->toStructure() as $data):
        ?>
            <?php 
                $unitColor;
                switch ($dataIndex) {
                    case 1:
                        $unitColor = 'violet';
                        break;
                    case 2:
                        $unitColor = 'bleu';
                        break;
                    case 3:
                        $unitColor = 'orange';
                        break;
                    case 4:
                        $unitColor = 'rose';
                        break;
                }       
            ?>
                <span class="data-category" data-color="<?= $unitColor ?>" data-number="<?= $data->percent() ?>" data-unit="%" data-category="<?= $data->tag() ?>"><!--
                    <?php for ($i = 0 ; $i < $data->percent()->toInt() ; $i++): ?>
                    --><span class="graph__unit"></span><!-- 
                    <?php endfor ?>
                    --></span>
            
            <?php $dataIndex++ ?>
        <?php endforeach ?>
        </div>
    </div>
    
    <figcaption class="font-mono h-100">
        <?php 
            $legendIndex = 1;
            foreach($block->data()->toStructure() as $data): 
        ?>
        <?php 
            $legendColor;
            switch ($legendIndex) {
                case 1:
                    $legendColor = 'violet';
                    break;
                case 2:
                    $legendColor = 'bleu';
                    break;
                case 3:
                    $legendColor = 'orange';
                    break;
                case 4:
                    $legendColor = 'rose';
                    break;
            }       
         ?>


            <p class="flex items-center">
                <span class="graph__unit bg-<?= $legendColor ?>"></span>
                <?= $data->tag() ?>
            </p> 
            <?php $legendIndex++ ?>   
        <?php endforeach ?>
    </figcaption>
</figure>