<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php if ($page->isHomePage()) { echo 'Études'; } else { echo $page->title(); } ?> - <?= $site->title() ?></title>
    <meta name="description" content="<?= $site->description() ?>">
    <link rel="preload" href="<?= url('assets') ?>/fonts/Planc-Book.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="<?= url('assets') ?>/fonts/Planc-Medium.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="<?= url('assets') ?>/fonts/Planc-Bold.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="<?= url('assets') ?>/fonts/Redaction_20-Regular.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="stylesheet" href="<?= url('assets') ?>/dist/style.css">


    <!--========== FAVICON ==========-->
    <link rel="apple-touch-icon" sizes="180x180" href="./apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./favicon-16x16.png">
    <link rel="manifest" href="./site.webmanifest">
    <link rel="mask-icon" href="./safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <meta property='og:title' content='<?= $site->title() ?>' />
    <meta property='og:type' content='website' />
    <meta property='og:url' content='<?= $site->url() ?>' />
    <meta property='og:image' content='<?php
    if ($site->socialImg()->isNotEmpty()) {
      echo $site->socialImg()->toFile()->url();
    }
    ?>' />
    <meta property='og:description' content='<?= $site->description() ?>' />

    <!--========== HTML2CANVAS ==========-->
    <script src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>

    <!--========== TINYCOLOR ==========-->
    <script type='text/javascript' src='<?= url('assets') ?>/js/libs/tinycolor-min.js'></script>
    
    <!--========== VUE ==========-->
    <!-- development version, includes helpful console warnings -->
    <script src="<?= url('assets') ?>/js/libs/vue.min.js"></script>
    <?php if ($page->isHomePage()): ?>
        <script src="<?= url('assets') ?>/dist/home.bundle.js" type="module" defer></script>
    <?php else: ?>
        <script src="<?= url('assets') ?>/dist/app.bundle.js" type="module" defer></script>
    <?php endif ?>

    <!--========== OPEN GRAPHS ==========-->
    <meta property='og:title' content='<?= $site->title() ?>' />
    <meta property='og:type' content='website' />
    <meta property='og:url' content='<?= $site->url() ?>' />
    <meta property='og:image' content='<?php
    if ($site->socialImg()->isNotEmpty()) {
      echo $site->socialImg()->toFile()->url();
    }
    ?>' />
    <meta property='og:description' content='<?= $site->desc() ?>' />
</head>

<body>
<!-- SVG icons -->
<?php snippet('symbols') ?>

<header id="header" role="banner" class="border-b<?php e($page->isHomePage(), ' header--home'); ?>">
  <a id="logo" href="/" aria-label="TMNlab" title="Accueil – TMNlab">
    <svg width="109" height="24" viewBox="0 0 109 24" fill="none" xmlns="http://www.w3.org/2000/svg" aria-labelledby="TMNlab-logo">
      <title id="TMNlab-logo">TMNlab</title>
      <g id="TMN">
        <rect width="2.5" height="2.5" fill="#6E3169"/>
        <rect y="3" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="3" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="3" y="3" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="6" y="3" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="6" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="6" y="6" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="6" y="12" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="6" y="18" width="2.5" height="2.5" fill="#D567AC"/>
        <rect x="6" y="9" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="6" y="15" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="6" y="21" width="2.5" height="2.5" fill="#D567AC"/>
        <rect x="9" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="9" y="6" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="9" y="12" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="9" y="18" width="2.5" height="2.5" fill="#D567AC"/>
        <rect x="9" y="3" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="9" y="9" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="9" y="15" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="9" y="21" width="2.5" height="2.5" fill="#D567AC"/>
        <rect x="12" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="12" y="3" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="15" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="15" y="3" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="21" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="45" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="21" y="3" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="45" y="3" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="24" y="3" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="42" y="3" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="21" y="6" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="42" y="6" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="21" y="12" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="42" y="12" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="21" y="18" width="2.5" height="2.5" fill="#D567AC"/>
        <rect x="42" y="18" width="2.5" height="2.5" fill="#D567AC"/>
        <rect x="21" y="9" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="39" y="9" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="42" y="9" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="21" y="15" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="42" y="15" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="21" y="21" width="2.5" height="2.5" fill="#D567AC"/>
        <rect x="42" y="21" width="2.5" height="2.5" fill="#D567AC"/>
        <rect x="24" y="6" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="45" y="6" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="24" y="12" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="45" y="12" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="24" y="18" width="2.5" height="2.5" fill="#D567AC"/>
        <rect x="45" y="21" width="2.5" height="2.5" fill="#D567AC"/>
        <rect x="24" y="9" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="45" y="9" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="27" y="6" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="27" y="9" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="39" y="6" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="36" y="9" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="30" y="9" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="33" y="9" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="24" y="15" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="45" y="15" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="30" y="12" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="36" y="12" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="33" y="12" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="33" y="15" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="24" y="21" width="2.5" height="2.5" fill="#D567AC"/>
        <rect x="45" y="18" width="2.5" height="2.5" fill="#D567AC"/>
        <rect x="51" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="69" width="2.50001" height="2.5" fill="#6E3169"/>
        <rect x="66" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="51" y="3" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="69" y="3" width="2.50001" height="2.5" fill="#6E3169"/>
        <rect x="54" y="3" width="2.49999" height="2.5" fill="#6E3169"/>
        <rect x="66" y="3" width="2.5" height="2.5" fill="#6E3169"/>
        <rect x="51" y="6" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="66" y="6" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="51" y="12" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="66" y="12" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="51" y="18" width="2.5" height="2.5" fill="#D567AC"/>
        <rect x="66" y="18" width="2.5" height="2.5" fill="#D567AC"/>
        <rect x="51" y="9" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="66" y="9" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="51" y="15" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="66" y="15" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="51" y="21" width="2.5" height="2.5" fill="#D567AC"/>
        <rect x="66" y="21" width="2.5" height="2.5" fill="#D567AC"/>
        <rect x="54" y="6" width="2.49999" height="2.5" fill="#40CCDF"/>
        <rect x="69" y="6" width="2.50001" height="2.5" fill="#40CCDF"/>
        <rect x="54" y="12" width="2.49999" height="2.5" fill="#DE7F3F"/>
        <rect x="69" y="12" width="2.50001" height="2.5" fill="#DE7F3F"/>
        <rect x="54" y="18" width="2.49999" height="2.5" fill="#D567AC"/>
        <rect x="69" y="18" width="2.50001" height="2.5" fill="#D567AC"/>
        <rect x="54" y="9" width="2.49999" height="2.5" fill="#40CCDF"/>
        <rect x="69" y="9" width="2.50001" height="2.5" fill="#40CCDF"/>
        <rect x="57" y="6" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="57" y="9" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="60" y="9" width="2.5" height="2.5" fill="#40CCDF"/>
        <rect x="54" y="15" width="2.49999" height="2.5" fill="#DE7F3F"/>
        <rect x="69" y="15" width="2.50001" height="2.5" fill="#DE7F3F"/>
        <rect x="60" y="12" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="63" y="12" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="63" y="15" width="2.5" height="2.5" fill="#DE7F3F"/>
        <rect x="54" y="21" width="2.49999" height="2.5" fill="#D567AC"/>
        <rect x="69" y="21" width="2.50001" height="2.5" fill="#D567AC"/>
      </g>
      <g id="lab">
        <path d="M77.0625 23.5114H78.5867V0H72.0032V1.26746H77.0625V23.5114Z" fill="#6E3169"/>
        <path d="M93.3952 23.6699C92.9411 22.2757 92.5195 20.7547 92.5519 19.1387L92.7465 12.2944C92.8438 8.55537 90.9628 6.40069 87.817 6.40069C86.1954 6.40069 84.736 6.8443 83.2442 7.41466L83.6009 8.77718C84.963 8.17513 86.2278 7.66815 87.7197 7.66815C90.5412 7.66815 91.3196 9.4426 91.2223 13.6886C90.0223 13.1182 88.4332 12.5796 87.0386 12.5796C83.7306 12.5796 81.8821 15.3363 81.8821 18.5366C81.8821 21.3567 83.5036 23.86 86.6495 23.86C88.8872 23.86 90.152 22.6876 91.352 21.0082C91.4493 21.9588 91.6763 22.9411 92.0006 23.86L93.3952 23.6699ZM91.125 19.2021C90.0872 20.9131 88.7899 22.5925 86.6495 22.5925C84.4766 22.5925 83.4063 20.5329 83.4063 18.6634C83.4063 16.1918 84.5414 13.847 87.3954 13.847C88.7575 13.847 90.0547 14.3223 91.125 14.861V19.2021Z" fill="#6E3169"/>
        <path d="M96.3013 0V17.6177C96.3013 19.6774 96.1391 21.7053 95.5229 23.6699L96.9499 23.86L97.5661 21.1033C98.4742 22.846 100.128 23.86 102.139 23.86C104.993 23.86 108.787 21.5469 108.787 15.0194C108.787 8.30188 104.409 6.40069 102.042 6.40069C100.517 6.40069 98.9931 6.90767 97.8256 7.82658V0H96.3013ZM97.8256 9.31585C98.8958 8.39694 100.193 7.66815 101.652 7.66815C103.598 7.66815 107.263 8.9673 107.263 15.1145C107.263 20.9448 104.052 22.5925 102.106 22.5925C99.8363 22.5925 98.6688 21.0399 97.8256 19.2337V9.31585Z" fill="#6E3169"/>
      </g>
    </svg>
  </a>
  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a itemprop="item" href="<?= $site->url() ?>">
        <span itemprop="name">Études</span>
      </a>
      <meta itemprop="position" content="1" />
    </li>
    <?php if (!$page->isHomePage()): ?>
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a itemprop="item" href="#" aria-current="true">
        <span itemprop="name"><?= $page->title() ?></span>
      </a>
      <meta itemprop="position" content="2" />
    </li>
    <?php endif ?>
  </ol>
  <?php if (!$page->isHomePage()): ?>
  <div class="ctas print:hidden">
    <button id="btn-intro" class="btn btn--outline btn--circle" title="Introduction">i</button>
    <button id="btn-tutorial" class="btn btn--outline btn--circle" title="Tutoriel">?</button>
  </div>
  <?php endif ?>
</header>