<svg display="none" xmlns="http://www.w3.org/2000/svg">
  <symbol id="icon-close" fill="none" viewBox="0 0 24 24">
    <path stroke="currentColor" d="M12 12L16 8M12 12L8 8M12 12L16 16M12 12L8 16" />
  </symbol>
  <symbol id="icon-link" fill="none" viewBox="0 0 24 24">
    <path stroke="currentColor" d="M10 7.5L7.5 7.5L7.5 16.5L16.5 16.5C16.5 16.5 16.5 15.3668 16.5 14M12 7.5L16.5 7.5L16.5 12" />
    <path stroke="currentColor" d="M10.5 13.5L16 8" />
  </symbol>
  <symbol id="icon-text" fill="none" viewBox="0 0 24 24">
    <path stroke="currentColor" d="M12 17.5v-11m0 11h-2m2 0h2m-2-11H6.5V9M12 6.5h5.5V9" />
  </symbol>
  <symbol id="icon-quote" fill="none" viewBox="0 0 24 24">
    <path stroke="currentColor" d="M6.5 7.5h4v4h-4zM13.5 7.5h4v4h-4zM10.5 12A4.5 4.5 0 0 1 6 16.5M17.5 12a4.5 4.5 0 0 1-4.5 4.5"/>
  </symbol>
  <symbol id="icon-graph" fill="none" stroke="currentColor" viewBox="0 0 24 24">
    <path d="M5.5 18L5.5 8"/>
    <path d="M8.5 18L8.5 12"/>
    <path d="M11.5 18L11.5 6"/>
    <path d="M17.5 18L17.5 10"/>
    <path d="M14.5 18L14.5 14"/>
  </symbol>
  <symbol id="icon-table" fill="none" stroke="currentColor" viewBox="0 0 24 24">
    <path d="M18.5 7.5L5.5 7.5L5.5 16.5L18.5 16.5L18.5 7.5Z" />
    <path d="M18.5 7.5L5.5 7.5L5.5 16.5L18.5 16.5L18.5 7.5Z" />
    <path d="M9.5 7.5V16.5M12.5 7.5V16.5M15.5 7.5V16.5M5.5 10.5H18.5M5.5 13.5H18.5" />
  </symbol>
  <symbol id="icon-bookmark" fill="none" viewBox="0 0 24 24">
    <path d="M16.5 7.5H7.5V17.5L12 14.25L16.5 17.5V7.5Z" stroke="currentColor"/>
  </symbol>
  <symbol id="icon-bookmark-fill" fill="currentColor" viewBox="0 0 24 24">
    <path d="M16.5 7.5H7.5V17.5L12 14.25L16.5 17.5V7.5Z" stroke="currentColor"/>
  </symbol>
  <symbol id="icon-part" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor">
    <path d="M10.5 9.5V7.5H6.5V9.5M10.5 9.5H17.5V16.5H6.5V9.5M10.5 9.5H6.5" stroke="currentColor"/>
  </symbol>
  <symbol id="icon-subpart" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor">
    <path d="M6 7.5L18 7.5"/>
    <path d="M6 10.5L18 10.5"/>
    <path d="M6 13.5L18 13.5"/>
    <path d="M6 16.5L14 16.5"/>
  </symbol>
  <symbol id="icon-visible" width="24" height="24" viewBox="0 0 24 24" fill="currentColor">
    <path d="M12.0148 17.0444C15.4345 17.0444 19.1349 15.0283 21.0999 12.3232V11.7617C19.1094 9.03109 15.4345 7.04053 12.0148 7.04053C8.56961 7.04053 4.86921 9.08213 2.92969 11.7617V12.3232C4.89473 15.0538 8.59513 17.0444 12.0148 17.0444ZM15.9194 15.1304C16.6084 14.2882 17.0167 13.1908 17.0167 12.0424C17.0167 10.894 16.6339 9.82221 15.9449 8.98005C17.4761 9.66909 18.8797 10.7664 19.9515 12.0424C18.8797 13.3184 17.4506 14.4413 15.9194 15.1304ZM8.11025 15.1304C6.55353 14.4413 5.12441 13.3184 4.05257 12.0424C4.71609 11.2768 6.11969 9.87325 8.05921 8.98005C7.39569 9.82221 6.98737 10.894 6.98737 12.0424C6.98737 13.1908 7.39569 14.2882 8.11025 15.1304ZM12.0148 13.446C11.2237 13.446 10.5857 12.7825 10.5857 12.0169C10.5857 11.2258 11.2237 10.5878 12.0148 10.5878C12.8059 10.5878 13.4439 11.2258 13.4439 12.0169C13.4439 12.7825 12.8059 13.446 12.0148 13.446Z" />
  </symbol>
  <symbol id="icon-invisible" width="24" height="24" viewBox="0 0 24 24" fill="currentColor">
    <path d="M19.0837 15.0794L21.2529 17.0444L21.9675 16.2788L19.7983 14.3138L19.0837 15.0794ZM2.75094 17.0444L4.89462 15.0794L4.18006 14.3138L2.03638 16.2788L2.75094 17.0444ZM18.4202 19.1626L19.3645 18.6267L17.8333 15.9726L16.9145 16.483L18.4202 19.1626ZM4.63942 18.6267L5.55814 19.1626L7.08934 16.483L6.17062 15.9726L4.63942 18.6267ZM15.0771 20.5917L16.0979 20.362L15.4344 17.2741L14.3881 17.5038L15.0771 20.5917ZM7.88046 20.362L8.92678 20.5917L9.61582 17.5038L8.5695 17.2741L7.88046 20.362ZM11.4788 21.0766H12.5251V17.81H11.4788V21.0766Z" />
    <path d="M12.0147 17.0444C15.4344 17.0444 19.1348 15.0284 21.0998 12.3232L20.2499 11.75C20.2499 11.75 16.9105 16 11.9999 16C7.08934 16 3.74994 11.75 3.74994 11.75L2.92958 12.3232C4.89462 15.0539 8.59502 17.0444 12.0147 17.0444Z" />
  </symbol>
  <symbol id="icon-minus" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor">
    <path d="M7 12H17"/>
  </symbol>
  <symbol id="icon-plus" viewBox="0 0 24 24" fill="none" stroke="currentColor">
    <path d="M7 12H17M12 7V17"/>
  </symbol>
  <symbol id="icon-up" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor">
    <path d="M16 14L12 10L8 14"/>
  </symbol>
  <symbol id="icon-down" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor">
    <path d="M16 10L12 14L8 10"/>
  </symbol>
  <symbol id="icon-annotation" viewBox="0 0 24 24" fill="none" stroke="currentColor">
    <path d="M18.5 7.5L5.5 7.5L5.5 16.5L10.5 16.5L12 18L13.5 16.5L18.5 16.5L18.5 7.5Z"/>
    <path d="M8 10.5L16 10.5"/>
    <path d="M8 13.5L13 13.5"/>
  </symbol>
  <symbol id="icon-callout" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor">
    <path d="M18.5 7.5L5.5 7.5L5.5 16.5L18.5 16.5L18.5 7.5Z"/>
    <path d="M8 10.5L16 10.5"/>
    <path d="M8 13.5L13 13.5"/>
  </symbol>
  <symbol id="icon-toc" width="24" height="24" viewBox="0 0 24 24">
    <circle cx="4.5" cy="7.5" r="1.5" fill="currentColor" style="fill: var(--fill)"/>
    <circle cx="4.5" cy="15.5" r="1.5" fill="currentColor" style="fill: var(--fill)"/>
    <circle cx="4.5" cy="11.5" r="1.5" fill="currentColor" style="fill: var(--fill)"/>
    <path d="M8 7.5L21 7.5" stroke="currentColor" style="stroke: var(--fill)"/>
    <path d="M8 11.5L21 11.5" stroke="currentColor" style="stroke: var(--fill)"/>
    <path d="M8 15.5L21 15.5" stroke="currentColor" style="stroke: var(--fill)"/>
  </symbol>
  <symbol id="icon-question" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5">
    <path d="M3.5 10.1783C3.5 13.0382 7.30558 15.3567 12 15.3567C12.2381 15.3567 12.4739 15.3507 12.707 15.339L14.756 19.1425L15.8285 14.8029C18.5998 13.9497 20.5 12.1992 20.5 10.1783C20.5 7.31842 16.6944 5 12 5C7.30558 5 3.5 7.31842 3.5 10.1783Z" style="stroke: var(--fill)" />
  </symbol>
  <symbol id="icon-collect" viewBox="0 0 24 24" fill="none" stroke="currentColor">
    <path d="M9.5 11L12 13.5L14.5 11" />
    <path d="M7.5 9V16.5H16.5V9" />
    <path d="M6 6C9.31371 6 12 8.68629 12 12V13" />
  </symbol>
  <symbol id="icon-expand" viewBox="0 0 24 24" fill="none" stroke="currentColor">
    <path d="M17.4999 18L6.50003 7.00007M17.4999 18L17.4999 12.5M17.4999 18L11.9999 18.0001M6.50003 7.00007L6.50003 12.5M6.50003 7.00007H12.0001" />
  </symbol>
  <symbol id="icon-carret-down" viewBox="0 0 24 24" fill="none" stroke="currentColor">
    <path d="M16 10L12 14L8 10" />
  </symbol>
</svg>
