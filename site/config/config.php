<?php

return [
    'debug' => true,
    'hooks' => [
        'page.create:after' => function($page) {
          $path = $page->contentFileDirectory() . '/data.json';
          $dataFile = fopen($path, 'w');
          fclose($dataFile);
        },
        'page.update:after' => function ($newPage, $oldPage) {
            if ($newPage->external()->exists() && $newPage->external()) return;
            
            // FIND CONCERNED STUDY
            $source = null;
            $isHome = $newPage->blueprint()->name() === 'pages/home';
            $isStudy = $newPage->blueprint()->name() === 'pages/study';
            $isAnnex = $newPage->blueprint()->name() === 'pages/annex';
            $isPart = $newPage->blueprint()->name() === 'pages/part';
            $isSubpart = $newPage->blueprint()->name() === 'pages/subpart';

            if ( $isStudy || $isHome) {
                $source = $newPage;
            } elseif ($isPart || $isAnnex) {
                $source = $newPage->parent();
            } elseif ($isSubpart) {
                $source = $newPage->parent()->parent();
            }

            // CREATE PREPARED JSON CONTENT
            $jsonContent = '';
            if ($isHome) {
                $jsonContent = prepareJsonHome($source);
            } else {
                $jsonContent = prepareJsonStudy($source);
            }

            // WRITE FILE
            $path = $source->contentFileDirectory() . '/data.json';
            $file = fopen($path, 'w');
            fwrite($file, $jsonContent);
            fclose($file);
        }
    ]
];