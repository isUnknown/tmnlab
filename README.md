# Présentation
Fait pour permettre d'étudier des études. Initialement designé et codé par le [Collectif Bam](https://www.collectifbam.fr) pour le [TMNlab](http://www.tmnlab.com/). 

## Principales fonctionnalités
Back-office : Ajout de contenus organisé par parties > sous-parties > blocs (_cf. $Types de blocs_). Ajout d'annexes (parties non numérotées). Ajout de ressources associées aux sous-parties. 
Front : Signets / marques-page, collecte de blocs (textes et graphs) en vue de les travailler et de les exporter et / ou imprimer. Fonctions d'export : graphs en fichier séparés ou étude complète. 

## Fonctionnement
[CMS Kirby](https://getkirby.com/) en back-office
[Framework Vue.js 2](https://v2.vuejs.org/) en front-office
Pour économiser les ressources du processeur et minimiser les échanges de données serveur - client, les données JSON sont générées lors de la mise à jour du back-office plutôt qu'au moment de la requête http.

## Déploiement
Télécharger ou cloner le répertoire. <br />
```npm install.``` <br />
Aller dans le panel : ```<url-racine>/panel``` <br />
Supprimer. Créer vos propres études.

## Utilisation du back-office CMS
L'outil est basé sur le CMS Kirby. Pour toute utilisation en ligne veuillez [acheter une licence (99€ par site à vie)](https://getkirby.com/buy).
Accédez au panel (back-office) via l'url www.url-de-votre-serveur.fr/panel.